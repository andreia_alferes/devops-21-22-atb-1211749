# Class Assigment 5 - Part 1

O objetivo da Parte 1 desta tarefa é praticar com Jenkins usando o projeto "gradle basic demo" (desenvolvido em CA2, Parte 1).

### Pipelines de CI/CD com Jenkins

## 1. Análise, Projeto e Implementação

#### 1.1 Configuração

- Comecei por instalar o ´Jenkins` no windows, e para isso segui o tutorial oficial https://www.jenkins.io/doc/book/installing/windows.

- Minha primeira etapa foi criar um arquivo Jenkins no ambiente de trabalho e de seguida abri a linha de comando e executei o comando `java -jar jenkins.war`.

- Fui então para a url `localhost:8080` para configurar minha conta Jenkins e configurar Jenkins com todos os plugins padrão.

- Após o "setup" ter sido concluido, concentrei-me no arquivo Jenkins. Para criar um novo pipeline, na página principal do Jenkins, selecionei ***New Item*** e selecionei ***pipeline***. Depois de nomear este pipeline (no meu caso chamado `CA5_PART1`). Fui a ***configure*** e selecionei a opção pipeline script e defeni as seguintes etapas do ***pipeline*** no script:

#### 1.2 Primeira etapa (Checkout)
Na primeira etapa serve para retirar o código do repositório.

````
stages {
stage('Checkout') {
steps {
echo 'Checking out...'
git 'https://Andreia_Alferes@bitbucket.org/andreia_alferes/devops-21-22-atb-1211749'
}
}
````
#### 1.3 Segunda e Terceira Fase (Assemble + Teste)
Neste estágio o assemble serve para compilar e produzir os arquivos com a aplicação e a fase de testes para executar os Testes Unitários e publicar no Jenkins os resultados dos testes.
````
stage('Assemble') {
steps {
dir ("CA2/part1/master"){
echo 'Assembling...'
bat './gradlew assemble'
        }
    }
}
stage('Test') {     
steps {
dir ("CA2/part1/master"){
echo 'Testing...'
bat './gradlew test'
        }
    }
}
````
#### 1.4 Quarta Etapa (Arquivamento)
Este estágio serve para arquivar os arquivos gerados durante a etapa de Montagem.
````
stage('Archiving') {
steps {
echo 'Archiving...'
dir ("CA2/part1/master"){
archiveArtifacts 'build/distributions/*'
            }
    }
 }
````
Quando executei o pipeline, todos os estágios foram executados com sucesso, após ter feito o build now, tal como podemos ver na imagem.

![](images/1.jpg)

Podemos também ver que os ficheiros .tar e .zip foram gerados com sucesso.

![](images/2.jpg)
#### 1.5 Criação de pipeline

No painel do jenkins, no item CA5_PART1 + Pipeline efetuei as seguintes alterações:

- *Pipeline Script do SCM* pois quero obter meu Jenkinsfile do meu repositório remoto.

- GIT, porque é o SCM

- URL do repositório: `https://Andreia_Alferes@bitbucket.org/andreia_alferes/devops-21-22-atb-1211749`

- Credenciais: Andreia Alferes/****** (Andreia Alferes Bitbucket)

- Caminho do script: CA5/PART1/JenkinsFile, pois como dito antes, meu Jenkinsfile não está na raiz do repositório.

![](images/5.jpg)

Nota: Nesta etapa foi necessário criar uma chave ssh para o jenkins aceder o bitbucket. Eu segui as etapas necessárias em bitbucket-sshKey e usei a chave ssh pública no bitbucket e o ssh privado no jenkins. No jenkinsFile foram colocados as `stages` acima a diferença é que o primeiro `stage` precisa ser como:
````
 stage('Checkout') {
 steps {
 echo 'Checking out...'
 git credentialsId: 'credenciaisAndreia',
  url: 'https://Andreia_Alferes@bitbucket.org/andreia_alferes/devops-21-22-atb-1211749'
            }
        }
````

- Efetuei o `Build Now` e tudo correu na perfeição, como mostra a imagem.

![](images/3.jpg)

Podemos também ver que os ficheiros .tar e .zip foram gerados com sucesso.

![](images/4.jpg)


## Terminar a tarefa com um tag

Foi criada a tag CA5-PART1 para finalizar a tarefa
````
git status
git add .
git commit -a -m"End of CA5 part1"
git pull
git push origin master 
git tag -a CA5PART1 -m"End of CA5 PART1"
git push origin CA5PART1
````
- Por lapso não coloquei no Stage test a funcionalidade para poder arquivar/publicar resultados dos testes e para isso fiz a seguinte alteração:

````
stage('Test') {     
 steps {
     dir ("CA2/part1/master"){
 echo 'Testing...'
 bat './gradlew test'
 junit 'build/test-results/test/*.xml'
        	}
        	
````

 Ao fazer o build surge a opção que permite consultar os test como mostra a imagem:

![](images/6.jpg)

 uma vez que foi feita uma nova alteração foi feito um novo tag a finalizar a tarefa:

 Criado novo tag CA5-Part1 para finalizar a tarefa

````
git status
git add .
git commit -a -m"End of CA5 part1"
git pull
git push origin master 
git tag -a CA5.part1 -m"End of CA5 Part1"
git push origin CA5.part1
````


