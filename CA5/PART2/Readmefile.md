## Tarefa de classe CA5 Part2
______

- O objetivo da Parte 2 desta tarefa é criar um pipeline no Jenkins para construir a aplicação de Spring boot, versão "básica" do gradle (desenvolvido em CA2, Part2).


## Pipelines CI/CD com Jenkins
___
##### Análise, Projeto e Implementação

* Para iniciar esta parte dois do CA5, criei um novo arquivo Jenkins em `./CA5/PART2`.
<br />
* Tal como foi executado na tarefa anterior comecei por aceder à linha de comandos a partir da pasta jenkins executei o comando `java -jar jenkins.war`.
<br />
* Em seguida, fui para o url localhost:8080 para configurar o Jenkins com todos os plugins padrão. Na verdade, esta etapa já foi concretizada na tarefa CA5 - parte1. 

##### Criar um pipeline

* No repositório bitbucket, na pasta CA5 PART2 que contém o projeto ,
um `Jenkinsfile` foi criado definindo as seguintes etapas:

   - **Checkout:** Necessário para retirar o código do repositório.

   - **Assemble:** Para compilar e produzir os arquivos com a aplicação.

   - **Test:** Para executar os Testes Unitários e publicar no Jenkins os resultados dos testes.

   - **Javadoc:** Para gerar o javadoc do projeto e publicá-lo no Jenkins. Além disso, foi necessário configurar todos os parâmetros necessários para a etapa publishHTML: 
    A etapa do Javadoc foi gerada usando a Sintaxe do Pipeline:
      ![](files/3.jpg)
<br />
    **Os significados de cada comando gerado são:**
  
   - `allowMissing: false`- o relatório não pode faltar para que o build funcione;
   - `alwaysLinkToLastBuild: false` - por ser false, o relatório Javadocs refere-se apenas ao último build que foi bem sucedido;
   - `keepAll: false` - arquiva apenas o relatório da última compilação;
   - `reportDir:` refere-se ao caminho, do diretório de trabalho, para o diretório com o relatório HTML;
   - `reportFiles:` refere-se ao arquivo que fornece os links dentro do diretório de relatórios especificado anteriormente;
   - `reportName:` o nome do relatório;
   - `reportTitles:` os títulos dentro do relatório (parâmetro opcional).
  
     <br />
   - **Arquivo:** Para arquivar os arquivos gerados durante a etapa de Montagem. Para     realizar esta tarefa o estágio de arquivo é um pouco diferente fase à tarefa anterior porque um arquivo war é necessário, caso contrário não seria capaz de arquivá-la. Para isso o plugin war foi adicionado ao build.gradle da tarefa CA2 part2.
`` plugins {id 'war'} ``

* Ao efetuar o build o mesmo foi realizado com sucesso
![](files/5.jpg)

  <br />

  - **Publicar imagem:** Para gerar uma imagem do docker usando um script para fazer login no Docker Hub e publicar as imagens geradas pelo Dockerfile neste repositório. Para concluir este estágio final foi necessário inicializar o Docker e fazer o login antes de executar o pipeline no Jenkins (para tal precisei de adicionar um novo conjunto de credenciais à minha conta Jenkins).Para criar novas credenciais, vou Dashboard -> Gerenciar credenciais -> Jenkins (lojas com escopo definido) -> Credenciais globais -> Adicionar credenciais, preencha o formulário com o nome de usuário e a senha da minha conta do hub do docker.
  ![](files/20.jpg)
  Foi também necessário instalar um plugin adicional o `Doker pipeline`.



**Um pipeline no Jenkins foi desenvolvido usando o seguinte pipeline script para a realização da tarefa, foi ligado ao URL do repositório bitbucket usando as credenciais salvas.**


>pipeline {
> <br />
>agent any
><br />
>stages {
> <br />
>stage('Checkout') {
> <br />
>steps {
> <br />
>echo 'Checking out...'
> <br />
>git credentialsId: 'credenciaisAndreia',
> <br />
>url: 'https://Andreia_Alferes@bitbucket.org/andreia_alferes/devops-21-22-atb-1211749'
>}}
><br />
><br />
>stage('Assemble') {
> <br />
>steps {
> <br />
>dir ("CA2/part2"){
> <br />
>echo 'Assembling...'
> <br />
>bat './gradlew assemble'
>}
>}
>}
><br />
><br />
>stage('Test') { 
> <br />
>steps {
> <br />
>dir ("CA2/part2"){
> <br />
>echo 'Testing...'
> <br />
>bat './gradlew test'
> <br />
>junit 'build/test-results/test/*.xml'
>}
>}
>}
><br />
><br />
>stage ('Javadoc'){
><br />
>steps{
> <br />
>dir ("CA2/part2"){
> <br />
>echo 'Publishing...'
> <br />
>bat './gradlew javadoc'
> <br />
>publishHTML([allowMissing: false,alwaysLinkToLastBuild: false,keepAll: false,reportDir: 'build/docs/javadoc',reportFiles: 'index.html',reportName: 'HTML Report', reportTitles: ''])
>}
>}
>}
> <br />
> <br />
>stage('Archiving') {
> <br />
>steps {
> <br />
>echo 'Archiving...'
> <br />
>dir ("CA2/part2"){
> <br />
>archiveArtifacts 'build/libs/*'
>}
>}
>}
> <br />
> <br />     
>stage('Docker Image') {
> <br />
>steps {
><br />
>echo 'Publishing Docker Image...'
> <br />
>dir('CA2/PART2') {
> <br />
>script {
> <br />
>docker.withRegistry('https://registry.hub.docker.com/', 'docker_credentials') {
> <br />
>def customImage=docker.build("devopsimage:${env.BUILD_ID}")
> <br />
>customImage.push()
> }}}}}}}

 

** Para realizar esta última etapa, um `Dockerfile` (como mostrado abaixo) foi incluído no CA2_part2. Esta etapa carrega
a imagem para o DockerHub com um prefixo CA5_part2 e a ID da compilação. **


>RUN apt-get update -y
> <br />
>RUN apt-get install -f
> <br />
>RUN apt-get install git -y
> <br />
>RUN apt-get install nodejs -y
> <br />
>RUN apt-get install npm -y
> <br />
>RUN mkdir -p /tmp/build
> <br />
>ADD build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/
> <br />
>EXPOSE 8082


<br />


- Por fim, executei a opção `Build Now` para executar o pipeline e foi bem sucedido.

![](files/6.jpg)

![](files/7.jpg)
<br />
- No Docker Hub foi possivel verificar a ciação da imagem bem sucedida.
![](files/10.jpg)


## 2. Analysis of an Alternative
_____

TeamCity, CircleCI, Travis CI, Bamboo , Buddy e Apache Maven são as alternativas e concorrentes mais populares do Jenkins.
<br />

**Jenkins vs Buddy**
<br />

| Características  | Jenkins| Buddy |
| -------- | -------- |-------- |
| Instalação| Requer instalação da aplicação Java  | Não necessita |
| Manutenção | Requer correção, ajuste e limpezas regulares | Não necessita|
|Integrações | Plugins de código aberto | Integrações pré-fabricadas e testadas |
|Pipelines | Com script e orientado à interface do usuário | com script e orientado à interface do usuário |
|Esforço | Requer instalação e manutenção regular | Sem necessidade de instalação ou manutenção |
|Custo | Gratuito | Gratuito com condição limitada |
|Costumização |Alto grau de controle, mas qualidade variável|Muitas opções altamente testadas para escolher, mas pouco controle sobre novas integrações |
|Conhecimento Especial | Requer pelo menos uma boa compreensão de trabalho de jenkins | Nenhum necessário |
<br />

**Jenkins vs Buddy**
<br />
- O Buddy é muito intuitivo/atrativo durante toda a sua implementação;
- Possibilita a sincronização com o Bitbucket e o Docker, tornando-se muito fácil de implementar; 
- O Buddy é mais fácil de trabalhar, no entanto é menos expansível;
- É um aplicativo com uma interface muito mais apelativa do que Jenkins.

***Em contrapartida:***
- O Jenkins tem uma comunidade gigantesca em comparação com o Buddy, o que facilita na resolução de problemas;
- O Jenkins torna-se mais personalizável da forma como trabalha com os plugins;


## Implementação da Alternativa
___

- Comecei por implementar a alternativa com a opção do bitbucket pipeline achando que a melhor opção no entanto deparei-me com alguns constrangimentos.

`Optei por implementar o Buddy.`

- Com Buddy, vou usar a aplicação web, para me registar sincronizei com minha conta bitbucket, esse processo disponibiliza todos os meus repositórios, e escolho criar um novo repositório [novo repositório](https://bitbucket.org:andreia_alferes/ca5.part2_alternativa.git) igual ao CA22 Part2, mas apenas para usar com o Buddy.

![](files/11.jpg)

- Após essa pequena configuração, comecei a replicar os trabalhos que já executei com o Jenkins:

##### Checkout
- Como o buddy já está sincronizado com minha conta bitbucket, só precisei escolher o repositório correto, não fiz uma tarefa específica para este trabalho. Acabei de iniciar meu projeto Buddy no repositório ca5.part2_alternativa que acabei de criar.

![](files/13.jpg)

#### Assemble, Test, Javadoc
- Buddy trabalha com ações em vez de stages, então assemble foi a 1ª acção que criei, onde primeiramente escolhi `Add action`.

![](files/14.jpg)

- De seguida o gradle.

![](files/15.jpg)

- Agora temos a janela bash e podemos escrever os comandos 'gradle build', 'gradle assemble','gradle test' e o 'gradle javadoc' mas primeiro precisamos alterar a versão do gradle, selecionando [alterar] para a versão do gradle em nosso projeto (gradle 7.4.1-jdk11).

![](files/16.jpg)

#### Docker

- Precisei de duas ações, uma para construir a imagem do docker e outra para enviar a imagem criada para o dockerhub.

- Para o primeiro, como o Dockerfile é a raiz do diretório, apenas selecionei a ação 'Build Docker Image' e salvei. 

![](files/17.jpg)

- Para a segunda parte, apenas escolhi a ação 'Push Docker image' e preenchi o formulário.

![](files/18.jpg)

- Depois de tudo isso pude correr o pipeline e verifiquei que correu tudo bem.
![](files/12.jpg)

- Verifiquei que a imagem foi efetivamente enviada para o dockerHub
![](files/19.jpg)

## Fim da tarefa CA5 PART2 com TAG

>git status
><br />
> git add .
><br />
> git commit -a -m"End of CA5 PART2. Resolved #48"
><br />
> git pull
><br />
> git push origin master
><br />
><br />
> git tag -a CA5 PART2 -m "End of CA5 PART2"
><br />
> git push origin CA5 PART2



