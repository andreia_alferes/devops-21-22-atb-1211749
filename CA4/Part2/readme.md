##CA4 part2 

## 1. Análise, Projeto e Implementação

O objetivo desta tarefa é usar o Docker para configurar um ambiente num container para executar a nossa versão do gradle
versão do aplicativo tutorial básico do spring, ou seja, devemos produzir uma solução semelhante à da parte 2 da tarefa anterior, mas agora usando o Docker em vez do Vagrant.

### Começando

já com o docker desktop instalado, uma vez que foi utilizado para o trabalho anterior.
O docker desktop é uma aplicação fácil que permite criar e compartilhar aplicativos e microsserviços em containers. O Docker Desktop inclui Docker Engine, cliente Docker CLI, Docker Compose, Docker Content Trust, Kubernetes e Credential Helper.

Criei dentro do diretorio **C:\Devops\devops-21-22-atb-1211749\CA4**, a pasta CA4 e duas subpastas a db e a web.

Para este projeto foi usado o exemplo de composição que configura os dois containers que está disponivel em:
https://bitbucket.org/atb/docker-compose-spring-tut-demo/
*Criei o docker-compose para produzir 2 serviços/containers:
**web:** este container é usado para executar o Tomcat e o aplicativo spring.
**db:** este container é usado para executar o banco de dados do servidor H2.

*Dentro da pasta web e db criei o Dokerfile.
*No diretório principal também foi criada a pasta data.
![0](pictures/pasta0.jpg)

*Todos os arquivos foram baixados e copiados para uma nova pasta CA4 part2
````
git status
git add .
git commit -a -m"Iniciate Ca4 part2.Addresses #46"
git pull
git push origin master
````

### Editando o Dockerfile da pasta da web 


A configuração das imagens do Docker pode ser automatizada usando Dockerfiles. O Dockerfile (web) foi atualizado
com as seguintes alterações:

* O Tomcat é um servidor web Java foi alterado:
````
FROM tomcat:9-jdk11-openjdk-slim
````

* O repositório a ser clonado foi alterado:
````
RUN git clone https://atb@bitbucket.org/atb/tut-basic-gradle-docker.git
````
* O diretório de trabalho foi alterado para a pasta onde está o projeto gradle:
````
WORKDIR /tmp/build/tut-basic-gradle-docker
````
* Tmbém foi adicionado o próximo comando no meu Dockerfile antes de ser executado *./gradlew clean build*.
````
RUN chmod u+x gradlew
````
* O nome do arquivo war foi modificado para corresponder ao do diretório do projeto:
````
RUN cp build/libs/basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/
````
* Todas as alterações foram gravadas.

### Construindo as imagens
* Na linha de comando, mudei para o diretório CA4 onde está o arquivo docker-compose.YAML.

* Em seguida, executamos o `docker-compose build` e a construção foi bem sucedida.
![0](pictures/build.jpg) 
* Executei o `docker-compose up` para iniciar os dois containeres.
![img.png](img.png)
### Testando o URL

* No host podemos abrir a aplicação web spring usando a seguinte url:
```
http://localhost:8080/basic-0.0.1-SNAPSHOT/
```
![img_1.png](img_1.png)
* Podemos abrir o console H2 usando a seguinte url:
```
http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console
> Para a string de conexão usamos: jdbc:h2:tcp://192.168.33.11:9092/./jpadb
```
![img_2.png](img_2.png)
![img_3.png](img_3.png)
![img_4.png](img_4.png)
### Publicando as imagens (db e web) no Docker Hub
* Abri o `https://hub.docker.com/` e entrei com os meus dados.
Para **db:** 
* Na linha de comandos executei `docker login`.
* Marcamos a imagem da web com `docker tag part2_db:latest aaalferes/part2_db:latest`.
* Nós enviamos a imagem db para o repositório do Docker Hub com `docker push part2_db:latest aaalferes/part2_db:latest`.
![img_5.png](img_5.png)
![img_6.png](img_6.png)
![img_7.png](img_7.png)
Para **web:** 
* Na linha de comandos executei `docker login`.
* Marcamos a imagem da web com `docker tag part2_web:latest aaalferes/part2_web:latest`.
* Nós enviamos a imagem db para o repositório do Docker Hub com `docker push part2_web:latest aaalferes/part2_web:latest`.
![img_8.png](img_8.png)
![img_9.png](img_9.png)
![img_10.png](img_10.png)
### Usando um volume com o container db para obter uma cópia do arquivo de banco de dados

Os volumes são o mecanismo preferencial para a persistência de dados no Docker.

* Na linha de comandos executei o comando `docker ps`, onde foram listadas as imagens.
![0](pictures/copy.jpg)
* Queremos copiar o arquivo `jpadb.mv.db` para a pasta `/usr/src/data-backup` porque no arquivo docker-compose.YAML era
definido para o serviço db:

```
volumes:
      - ./data:/usr/src/data-backup
```
* Compartilha a pasta ./data no host com a pasta /usr/src/data no container.
* Agora iniciei o container db e vamos para dentro do container onde executei `docker exec -it f17262a984d5 bash`.
* Agora para fazer uma cópia do arquivo de banco de dados no nosso host usei o comando `cp ./jpadb.mv.db /usr/src/data`.
![0](pictures/copy2.jpg)
![0](pictures/copy3.jpg)
## 2. Análise de uma alternativa

### O que é Docker?
![0](pictures/docker.jpg)
O Docker foi lançado em 2013 pela Docker, Inc. como uma plataforma de conteinerização de código aberto. Ele prometeu uma maneira fácil de criar e implantar containers na nuvem ou no local e é compatível com Linux e Windows. Embora o Docker não tenha introduzido um novo conceito, sua 'nova maneira de implantar software' e mensagens de 'tempo de comercialização mais rápido' atraíram tanto os usuários que o Docker logo se tornou uma abreviação para containers e o formato padrão de container.
O Docker simplifica a criação de containers com ferramentas como dockerfile e docker-compose . Ele também ajuda os desenvolvedores a mover cargas de trabalho de seu ambiente local, para testar até a produção, removendo as inconsistências e dependências entre ambientes. Isso resulta em uma entrega mais rápida do software e um aumento na qualidade. 

### O que é Kubernetes?
![0](pictures/kubernetes.jpg)
Kubernetes um projeto de código aberto disponibilizado pelo Google em 2014. O Kubernetes é um orquestrador de plataformas de contêiner, como o Docker. O Kubernetes permite que os usuários definam o estado desejado da implantação de sua arquitetura de contêiner em vários substratos. Após a entrada do usuário, o Kubernetes pode implantar e gerenciar aplicativos de vários contêineres em vários hosts, tomando medidas, se necessário, para manter o estado desejado. Esse nível de automação revolucionou o espaço do contêiner, pois criou a estrutura para recursos como escalabilidade, monitoramento e implantações entre plataformas.

**Docker vs Kubernetes**
![0](pictures/alternativa.jpg)
A maneira certa de pensar sobre essas tecnologias não é “Kubernetes vs Docker”, mas sim “Kubernetes e Docker”.
Kubernetes e Docker trabalham juntos para orquestrar um aplicativo de software. Os containers do Docker servem como o instrumentos, cada um fornecendo uma única parte do todo. Kubernetes é o maestro da orquestra, garantindo que todas as peças estão afinadas e tocando no tom certo. O Docker é usado principalmente durante os primeiros dias de um aplicativo em contêiner. Na verdade, ajuda a construir e implantar o(s) contêiner(es) do aplicativo. Nos casos em que a arquitetura do aplicativo é bastante simples, o Docker pode atender às necessidades básicas do gerenciamento do ciclo de vida do aplicativo. 
Nos casos em que o aplicativo é dividido em vários microsserviços, cada um com seu próprio ciclo de vida e necessidades operacionais, o Kubernetes entra em ação. O Kubernetes não é usado para criar os contêineres do aplicativo; ele realmente precisa de uma plataforma de contêiner para ser executado, sendo o Docker o mais popular. O Kubernetes se integra a um grande conjunto de ferramentas criado para e em torno de contêineres e o usa em suas próprias operações. Os contêineres criados com o Docker ou qualquer uma de suas alternativas podem ser gerenciados, dimensionados e movidos pelo Kubernetes, o que também garante o gerenciamento de failover e a manutenção da integridade do sistema. 
![0](pictures/ciclo.jpg) 
Há grandes benefícios em usar o Kubernetes com o Docker:
* Os aplicativos são mais fáceis de manter, pois são divididos em partes menores
* Essas partes são executadas em uma infraestrutura mais robusta e os aplicativos são mais altamente disponíveis
* Os aplicativos são capazes de lidar com mais carga sob demanda, melhorando a experiência do usuário e reduzindo o desperdício de recursos. À medida que os aplicativos se tornam mais escaláveis, não há necessidade de pré-alocar recursos antecipando os horários de pico de carga.

## Implementação da Alternativa

Não foi possivel implementar o kubernetes, no entanto ficam os passos necessários para a sua implementação:

* Iniciava com a instalação do kubernetes e minikube.
* Após a instalação faria a implantação tanto do db quanto da web a partir das imagens do dockerHub usadas na implementação do Docker.
````
kubectl create deployment k8sdb --image=switchlr/devops:dbpush

kubectl create deployment k8web --image=switchlr/devops:webpush
````
* Para verificar se tudo correu bem na implantação executava `kubectl get deployment`.
* Para verificar se os dois pods estão em execução executava `kubectl get pods`.
* para converter o docker-compose.yaml para objetos kubernetes utilizaria o kompose. Depois de instalar o kompose na pasta onde tinha o  docker-compose.yaml eu executaria o comando `kompose converter`.
Este comando iria gerar vários arquivos conhecidos pelo kubernetes.

* Algumas mudanças seriam necessárias nos seguintes arquivos:
1. Em `db-deployment.yaml` e `web-deployment.yaml` eu precisava de adicionar as imagens do meu dockerhub.
2. Nos arquivos `db/service.yaml` e `web/service.yaml` eu precisava
especificação do tipo `NodePort`.
3. Na política de rede padrão `apiVersão: networking.k8s.io/v1`.

* Neste ponto iniciaria o minikube com `minikube start`.Com este comando, o vm criado é baseado no docker.
* Após isso, seria necessário aplicá-las com o comando `kubectl apply -f`. Seria possivel ver o IP de cada pod com o `kubectl get pod -o wide`.
Entrava no web pod, através do comando `kubectl exec -it f17262a984d5 bash`.
* Após implementação do docker executava `./gradlew clean build` e `cp build/libs/basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/`.
* Depois de sair do pod, iria tentar aceder ao db e para isso primeiro precisaria de iniciar o serviço através do comando `minikube service db`. 
* Abriria automaticamente uma janela do navegador com o login da console h2, no login precisaria de alterar o URL para usar o endereço IP correto. Poderia ter acesso ao banco de dados h2.
* De seguida tentaria aceder ao web e para isso iria executar o comando `minikube service web`.
* Se tudo corresse como espectável, poderiamos ver a tabela esperada no browser.









  