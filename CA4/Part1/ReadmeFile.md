Tarefa CA4 - PART1
===================
-----

## 1. Análise, Projeto e Implementação


O objetivo desta tarefa é usar o Docker tarefa é praticar com o Docker, criando docker
images e containers em execução usando o aplicativo "chat application" do CA2.

Primeiro procedi à instalação do **Docker Desktop**  através de 'https://docs.docker.com/desktop/windows/install/' , no meu computador.
![img.png](img.png)

Após ter sido efetuada a instalação, na linha de comandos ubuntu foram executados os seguintes comandos.
![img_1.png](img_1.png)

````bash
sudo apt update
apt --list upgradable

Também foi executado docker pull ubuntu:18.04
````

Para explorar o conceito de imagens do docker, foram criadas duas versões (v1 e v2):

**v1:**
O objetivo desta versão é construir o servidor de chat "dentro" do Dockerfile e para isso fiz os seguintes passos:

No meu computador começei por criar a pasta CA4 e a pasta part1 e a pasta v1.
![img_13.png](img_13.png)
O ficheiro Dockerfile foi criado na linha de comandos BASH, para tal foi executado o comando,
````
touch Dockerfile 
````
Após ter sido criado procedi à edição do Dockerfile (para a v1)
````
# Image
FROM ubuntu:18.04
RUN apt-get update
# ADD LAYERS
# Install java
RUN apt-get install -y openjdk-11-jdk-headless
# Install git
RUN apt-get install -y git
# Clone repository
RUN git clone https://andreia_alferes@bitbucket.org/luisnogueira/gradle_basic_demo.git

# Change directory
WORKDIR gradle_basic_demo/
RUN chmod u+x gradlew
RUN ./gradlew clean build

# Server port
EXPOSE 59001
# Build container
CMD java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
````

Na linha de comandos executei 
````
docker build -t ca4 .
````
![img_2.png](img_2.png)


Na mesma linha de comandos executei
````
docker run -p 59001:59001 --name ca4 -d ca4
````
![img_4.png](img_4.png)
![img_5.png](img_5.png)

No diretório CA2/Part1/master abri a linha de comandos e fiz **gradlew runClient**.
![img_6.png](img_6.png)

Foi executado com sucesso 
![img_7.png](img_7.png)

Marquei a imagem e publiquei-a no docker hub, para tal executei na linha de comandos (como podemos ver nas imagens).
````
docker login
docker tag ca4 ca4:ca4part1
docker push aaalferes/ca4:ca4part1
````
![img_8.png](img_8.png)

Confirmaçao no docker
![img_9.png](img_9.png)
![img_10.png](img_10.png)

Por fim executei o comando abaixo para listar os containers em uso.
````
docker container ls
````
![img_11.png](img_11.png)

Depois executei o comando de forma a parar o container que estava em utilização.
````
docker stop ca4
````
![img_12.png](img_12.png)

**v2:**
O objetivo desta versão é construir construir o servidor de chat no meu computador host e copiar o jar
arquivo para o Dockerfile criado.

No meu computador começei por criar a pasta CA4 e a pasta part1 e a pasta v2 já com o ficheiro **basic_demo-0.1.0.jar**.
![img_14.png](img_14.png)
O ficheiro Dockerfile foi criado na linha de comandos BASH, para tal foi executado o comando,
````
touch Dockerfile 
````
Após ter sido criado procedi à edição do Dockerfile (para a v2)
````
# Image
FROM openjdk:11

# Change directory
WORKDIR /root

# Clone
COPY basic_demo-0.1.0.jar .

# Server port
EXPOSE 59001

# Build container
CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
````

Após ter sido editado o Dockerfile referente ao v2, no diretório CA4/Part1/v2 abri a linha de comandos e executei.
````
docker build -t file2 .
````

Na mesma linha de comandos executei
````
docker run -p 59001:59001 -d file2
````
![img_15.png](img_15.png)

Ao aceder à imagem consegui verificar que a tarefa foi efetuada com sucesso
![img_16.png](img_16.png)

Marquei a imagem e publiquei-a no docker hub, para tal executei na linha de comandos (como podemos ver nas imagens).
````
docker login
docker tag file2:latest aaalferes/file2:latest
docker push aaalferes/file2:latest
````

Confirmaçao no docker
![img_18.png](img_18.png)
![img_19.png](img_19.png)

Por fim executei o comando abaixo para listar os containers em uso.
````
docker container ls
````

De seguida executei o comando de forma a parar o container que estava em utilização.
````
docker stop 764ab1863298
````
![img_20.png](img_20.png)

De forma a toda esta informação ficar no meu repositorio executei.
````
git status
git add .
git commit -a -m"Iniciate assignment CA4 part1"
git commit -a -m"Create v2 to the assignment CA4"
git commit -a -m"Create ReadmeFile CA4 part1"
git commit -a -m"End of CA4 part1"
git pull
git push origin master 
````

Para marcar o fim da part1 da tarefa CA4 foi feito um tag e para tal foi executado.
````
git tag -a CA4Part1 -m"End of CA4 part1"
git push origin CA4Part1
````

Nota: Todos os procedimentos efetuados ao longo da execução da tarefa foram registadas no ficheiro readme file.








