Tarefa CA2 - PART1
-------------------
-------------------

Criação do Projeto
------------------

**1.** Fiz Download do Repositório que se encontrava no https://bitbucket.org/luisnogueira/gradle_basic_demo/src/master/

**2.** Criei uma pasta CA2 coloquei toda a info do Repo
(c:\Devops\devops-21-22-atb-1211749\CA2)

**3.** Executei:
```` bash
    git status
    git add .
    git commit -m "Add part1 CA2"
    git push origin
````
**4.** Ao ter feito o push verifiquei que não assignei ao issue que tinha criado, então executei :
``` bash
    git status
    git add .
    git commit -m "Start part1 of CA2, Resolved #18"
    git push origin
````
**5.** Executei o ficheiro Readme, para tal utilizei os seguintes comandos:
``` bash
    echo "Hello World" >>Readme.md
    git status
    git add Readme.md
    git commit -m "Add Readmefile, Resolved #19"
    git push origin
````
**6.** Seguindo o ficheiro Readme que nos foi disponibilizado, para construir um arquivo .jar com o aplicativo, o seguinte comando foi executado:
``` bash
    gradlew build
```
![img.png](img.png)

**7.** Para executar o servidor, o seguinte comando foi executado do diretório raiz do projeto:
``` bash
    java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
```
Ao pedir para executar este comendo foram dados vários erros no terminal, nomeadamente:
![img_9.png](img_9.png)

Para resolver esta questão foi iniciado um novo repositório na pasta CA2 part1 utilizando o comando:
``` bash
    git clone https://bitbucket.org/luisnogueira/gradle_basic_demo/src/master/
```
posteriormente executei na linha de comandos:
``` bash
    git status
    git add .
    git commit -m "Create CA2 part1 and readme file, Resolved #20"
    git push origin
```
Resolvida a questão anterior e seguindo o Readme file disponibilizado foram executados no terminal os seguintes comandos:
``` bash
    gradlew build (para construir o arquivo)
    java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001 (para executar o servidor)
    gradlew runClient (esta tarefa foi executada num novo terminal)
```
Após a mensagem de compilação de sucesso, uma janela de conversação foi aberta. Isso permitiu-me criar uma sala de conversação no meu computador.

**8.** Foi criado um novo ficheiro Readmefile:
``` bash
    echo "Hello World" >>Readme.md
    git status
    git add Readme.md
    git commit -m "Create a ReadmefileCa2-part1.md, Resolved #22"
    git push origin
```
**9.** Criação de uma tarefa nomeada **runServer**

* Abri o ficheiro build.gradle
* Criei uma tarefa chamada runServer com base na tarefa runClient

``` java  
    task runServer(type:JavaExec, dependsOn: classes){
     group = "DevOps"
    description = "Launches a chat client that connects to a server on localhost:59001 "

    classpath = sourceSets.main.runtimeClasspath

    mainClass = 'basic_demo.ChatServerp'

    args '59001'
    }
```
De seguida executei:
``` bash
    git status
    git add .
    git commit -m "Add new task to execute the server, Resolved #21"
    git push origin
 ```   
Para verificar se o código está correto, existem duas formas para poder validar:

*a)* Listar as tarefas disponíveis, através do comando:
``` bash  
    gradlew tasks --all
``` 
*b)* Executando o servidor, através do comando:
``` bash 
    % gradlew runServer 
```   
![img_1.png](img_1.png)

Este comando teve a mesma saída que o executado anteriormente para executar o servidor.

De seguida procedi ao registo da tarefa utilizando os seguintes comandos:
``` bash
    git status
    git add .
    git commit -m "Listing the available tasks and running the server,Resolved #23"
    git push origin
```
**10.** Testar o aplicativo

*a)* Criei um arquivo AppTest em `src/test/java/basic_demo/AppTest.java`

Procedi ao registo da tarefa utilizando os seguintes comandos:
``` bash
    git status
    git add .
    git commit -m "Add new file in SRC named AppTest.java and insert dependency,Resolved #24"
    git push origin
```
*b* Criei um teste unitário para o método getGreeting() no arquivo Test:
```` java
public class AppTest {
    @Test public void testAppHasAGreeting() {
        App classUnderTest = new App();
        assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    }
}
````
* Adicionei uma dependência na pasta build.gradle (linha 28)  
```` java
implementation 'junit:junit:4.12'
````
Foi feito o registo no repositório, através dos comandos:
```` bash
    git status
    git add .
    git commit -m "Add test for getGreeting(), Resolved #25"
    git push origin
````    
Para verificar se estava tudo em ordem, foram executados os comandos:
```` bash
    gradlew build
    gradlew test 
   ````
![img_3.png](img_3.png)

Foi feito o registo no repositório utilizando os seguintes comandos:
```` bash
    git status
    git add .
    git commit -m "Run AppTest.java with gradlew test,Resolved #26"
    git push origin
  ````
**11.** Criação da tarefa copyTask
* Abri o ficheiro build.gradle
* Criei uma tarefa chamada backupCopy do tipo Copy
```` java
task backupCopy(type: Copy) {
    from 'src'
    into 'backup'
}
````
Esta tarefa copia todos os arquivos em `src` para uma pasta chamada `backup` e para tal foi executado os comandos no terminal:
```` bash
    gradlew build
    gradlew backupCopy
````
![img_4.png](img_4.png)

Foi feito o registo no repositório, através dos comandos:
```` bash
    git status
    git add .
    git commit -m "Create a copy task (new folder named backup in src), Resolved #27"
    git push origin
````
**12.** Criação da tarefa zipFile
* Abri o ficheiro build.gradle
* Criei uma tarefa chamada zipFyle do tipo Zip
```` java
task zipFile (type:Zip) {
    from 'src'
    archiveFileName = 'src.zip'
    destinationDirectory = file('zipfiles')
}
````
Esta tarefa copia o conteúdo da pasta `src` para uma nova pasta chamada `zipfiles` e para tal foi executado os comandos no terminal:
```` bash
    gradlew build
    gradlew zipFile
````
![img_5.png](img_5.png)
![img_12.png](img_12.png)

Foi feito o registo no repositório, através dos comandos:
```` bash
    % git status
    % git add .
    % git commit -m "Add task type ZIP, Resolved #28"
    % git push origin
````
**13.** Adicionar o  tag ca2-part1 para finalizar a part1

Foi criado um tag para marcar o fim desta tarefa:
``` bash
git tag -a ca2-part1 -m "End of ca2-part1"
git push origin ca2-part1
```

Todas as mudanças implementadas acima foram submetidas ao repositório usando o Git, tal como foi demonstrado ao longo da execução do Readmefile.

**Nota:** Todos os passos determinantes para a execução da tarefa CA2, Part 1 foram registadas neste mesmo documento e de forma a ficarem disponiveis para visualização o ficheiro ReadmefileCa2 foi submetido ao repositório, utilizando os seguintes comandos:
```` bash
    git status
    git add .
    git commit -m "Add changes on ReadmefileCa2"
    git push origin
````
