#Class Assignment 2
##CA2, Part 2 - Build Tools with Gradle

1. Para dar inicio à tarefa foi criada a pasta part2 em (c:\Devops\devops-21-22-atb-1211749\CA2\part2).
Foi executado 

```` bash
    git status
    git add .
    git commit -m "Create a new folder part2, Resolved #30"
````

2. Criei o novo branch ***tut-basic-gradle*** e para isso executei 

```` bash
    git branch tut-basic-gradle
    git branch (Head -> Master)
    git checkout tut-basic-gradle
    git status (Head -> tut-basic-gradle)
    git add .
    git push --set-upstream origin tut-basic-gradle
    git pull
````

>**NOTA:**
> 
> O branch também poderia ser executado através do comando ***git branch -b tut-basic-gradle***

![img_1.png](img_1.png)
![img_2.png](img_2.png)


3. Criei ficheiro Readme, para tal utilizei os seguintes comandos:

```` bash
    echo "Hello World" >> Readme.md
    git status
    git add . ReadmeCa2Part2.md
    git commit -m "Add readme file and create a new branch tut-basic-gradle, Resolved #29 and #31"
    git push origin tut-basic-gradle 
````

4. Conforme indicado, usei o https://start.spring.io, de forma a poder iniciar um novo projecto gradle com as seguintes dependências:
    * Rest Repositories
    * Thymeleef
    * JPA
    * H2

Descarreguei a pasta zip e passei o seu conteúdo para a pasta ca2/part2

5. Tenho agora a aplicação spring "vazia" que pode ser construida usando o gradle.
Verifico as tarefas de gradle disponíveis usando o comando:

```` bash
    gradlew task 
   ````

![img.png](img.png)

6. Apaguei a pasta SCR, de seguida executei:

```` bash
    git status
    git add .
    git commit -m "Delet Scr folder, Resolved #32"
    git push origin tut-basic-gradle
   ````
7. Copiei do ficheiro tut-react-and-spring-data-rest a pasta src e as subpastas para a pasta ca2-part2.


8. Copiei o arquivo webpack.config.js e package.json para a raiz do projeto que criei, de seguida executei:

```` bash
    git status
    git add .
    git commit -m "Add SRC file, webpack.config.js and package.js, Resolved #33"
    git push origin tut-basic-gradle
   ````
9. Eliminei a pasta src/main/resources/static/built/ já que esta pasta deve ser
   gerada no javascript pela ferramenta webpack.


10. Executei o comando:
```` bash
    gradlew bootRun
   ````
![img_4.png](img_4.png)

11. Ao correr o localhost este não dá qualquer informação. Copiamos o código java e o frontend mas o gradle ainda não está a fazer o processamento dos ficheiros javaScript.

![img_5.png](img_5.png)

12. Ao ficheiro frontend foi adicionado um plugin e colocamos na secção de plugin na pasta build.gradle (linha 5)

   
      id "org.siouan.frontend-jdk11" version "6.0.0"


![img_6.png](img_6.png)

13. Para configurar o plugin anterior também tivemos de proceder às seguintes alterações no build.gradle.

````javascript
frontend {
nodeVersion = "14.17.3"
assembleScript = "run build"
cleanScript = "run clean"
checkScript = "run check"
}
````

14. Também fiz a atualização do script section/object na package.json para configurar a execução do webpack.

````javascript
"scripts": {
"webpack": "webpack",
"build": "npm run webpack",
"check": "echo Checking frontend",
"clean": "echo Cleaning frontend",
"lint": "echo Linting frontend",
"test": "echo Testing frontend"
}
````

15. Após as alterações foi executados os comandos:

````bash
   gradlew build
````

![img_7.png](img_7.png)

````bash
   gradlew bootRun
````

Ao correr o http://localhost:8080/ numa página de internet foi observado:

![img_8.png](img_8.png)

16. Adicionei uma tarefa ao gradle para excluir todos os arquivos gerados pelo webpack (localizado em
    src/resources/main/static/built/). Esta nova tarefa exclui automaticamente
    por gradle antes da limpeza da tarefa. 

````javascript
task jarCopy(type: Copy) {
	from 'build/libs'
	into 'dist'
}
````

![img_9.png](img_9.png)
![img_11.png](img_11.png)

Executei o comando,

````bash
   gradlew jarCopy
````

Registei os passos anteriores efetuando:

```` bash
   git status
   git add .
   git commit -m "add a task jarCopy, Resolved #34"
   git push origin tut-basic-gradle
````

17. Adicionei uma tarefa ao gradle.build para excluir todos os arquivos gerados pelo webpack. Ele geralmente está localizado em src/resources/main/static/built/.
A nova tarefa vai executar automaticamente antes da limpeza da da tarefa.
Comecei criando uma tarefa denomidada de deleteFiles.

````javascript
task deleteFiles {
doLast {
delete 'src/main/resources/static/built'
}
````
Executei o comando,

````bash
   gradlew deleteFiles
````
![img_10.png](img_10.png)
![img_12.png](img_12.png)

Criei tambem uma tarefa chamada cleanFiles que depende do deleteFiles

````javascript
task cleanFiles {
dependsOn deleteFiles
doLast {
clean
}
````
Executei o comando,

````bash
   gradlew cleanFiles
````
![img_13.png](img_13.png)
![img_14.png](img_14.png)

Registei os passos anteriores efetuando:

```` bash
   git status
   git add .
   git commit -m "add a task cleanFiles and deleteFiles, Resolved #34 and #35"
   git push origin tut-basic-gradle
````

18. fiz o gradlew build e o programa corre sem problema,

![img_15.png](img_15.png)
![img_16.png](img_16.png)

19. Como efetuei todas as tarefas relacionadas com a tarefa foi feito um merge com branch master.

```` bash
   git status
   git add .
   git commit -m "test the aplication and merge with the master branch, Resolved #36"
   git push origin tut-basic-gradle
   git checkout master
   git merge tut-basic-gradle
   git branch (Head -> Master)
````

20. Terminei a tarefa ca2-part2 e marquei a mesma com o tag ca2-part2

```` bash
   git status
   git add .
   git commit -m "end the assignment part2 and tag ca2-part2, Resolved #37"
   git push origin master
   git tag -a ca2-part2 -m "end the assignment part2 and tag ca2-part2"
   git tag
   git push origin ca2-part2
````
![img_17.png](img_17.png)


#Alternativa escolhida para realizar a part2 da tarefa foi o ***Maven***

1. Criei uma nova pasta part2-Maven (c:\Devops\devops 21-22-atb-1211749\CA2)
Passei o conteúdo do ficheiro tut-react-and-spring-data-rest para a pasta criada acima, foi passado o conteudo à exepção do .git.

Foram executados os seguintes comandos,

```` bash
   git status
   git add .
   git commit -m "start the assignment using Maven, "
   git push origin master
````

2. Criei um novo branch e para tal executei o comando,

```` bash
   git checkout -b tut-basic-maven
   git branch (Head -> tut-basic-maven)
   ````
3. Como o ficheiro original já é um ficheiro Maven inicie a tarefa no ponto 11 onde executei o comando,

```` bash
   mvnw install
   ````
![img_18.png](img_18.png)

de seguida executei o comando,

```` bash
   mvnw spring-boot:run
   ````
![img_20.png](img_20.png)

Após ter executado o spring-boot abri uma página web e coloquei o http://localhost:8080/ onde foi observado: 

![img_19.png](img_19.png)

4. Ao ficheiro pom.xml foi adicionado um plugin para poder copiar.
Verifiquei que estava a funcionar executando o mvn validate

![img_21.png](img_21.png)

Verifiquei que cada um dos ciclos de vida de criação são defenidos por uma lista de diferentes fases de criação.
Em que cada fase de criação representa um estágio no ciclo de vida.
Existem as seguintes fases validate, compile, test, package, verify, install, deplay.

5. Adicionei um novo plugin para eliminar a pasta src/main/resources/static/built.
No maven corri o mvn clean e a pasta é removida.

![img_22.png](img_22.png)

Executei,

```` bash
   git status
   git add .
   git commit -m "add plugin to create a folder backup and another to delete the folder built, Resolved #39"
   git push origin tut-basic-maven
````

6. Foi finalizada a tarefa fazendo o merge para o branch master e adicionado o tag ca2-part2-maven, para tal executei,

```` bash
   git status
   git add .
   git commit -m "End the assignment Part2 maven and tag ca2-part2maven, Resolved #40"
   git push origin tut-basic-maven
   git checkout master
   git merge tut-basic-maven
   git branch (Head -> Master)
   git tag -a ca2-part2maven -m "End the assignment part2maven and tag ca2-part2maven"
   git tag origin ca2-part2maven
````

![img_23.png](img_23.png)

## Ferramentas de automação de compilação

Ao desenvolver este trabalho apercebi-me de que muitas vezes os desenvolvedores precisam de reconstruir o mesmo código várias vezes. Para isso existem ferramentas de compilação disponíveis que são mais apropriadas para a compilação e automação. As ferramentas de construção predominamentes são:

    Apache Ant 
    Maven
    Gradle

###***Apache Ant***

É uma ferramenta de linha de comando baseada em Java que usa arquivos XML para definir scripts de construção. Ele é predominantemente usado para compilações Java, mas também pode ser usado para desenvolvimento C/C++. 
As tarefas integradas oferecem maneiras de compilar, montar, testar e executar aplicativos de software. Os usuários também podem criar suas próprias "antlibs" para aprimorar a funcionalidade do Ant. 
O desenvolvimento do Apache Ant começou em 2000.

**Vantagens:**
* Melhor controle sobre o processo geral de criação.
* Flexível o suficiente para trabalhar com qualquer processo de trabalho.

**Desvantagens:**
* Arquivos de construção baseados em XML podem crescer e se tornar insustentáveis.
* São necessários muitos recursos e tempo para manter os scripts de construção.
* A integração do IDE é difícil de alcançar

###***Maven***
Maven foi desenvolvido para resolver os problemas enfrentados com o script baseado em Ant.
Manteve os arquivos XML, mas adotou uma abordagem diferente para a organização. 
No Ant, os desenvolvedores precisam criar todas as tarefas. 
O Maven reduz a criação de tarefas implementando padrões mais fortes para organizar o código. Como resultado, é mais fácil começar a usar projetos padrão.
Também introduziu downloads de dependência que facilitaram o desenvolvimento. Antes da introdução da Ivy em Ant, os usuários tinham que gerenciar as dependências localmente. Maven adotou primeiro a filosofia de gerenciamento de dependência.
No entanto, os padrões estritos da Mavens dificultam a criação de scripts de construção personalizados. A ferramenta é fácil de trabalhar, desde que o projeto siga os padrões rigorosos.

**Vantagens:**
* Downloads automáticos de dependências.
* Todas as dependências são registradas automaticamente no controle do código-fonte como parte dos scripts Maven.
* Padroniza e simplifica o processo de criação.
* Integra-se facilmente a IDEs e sistemas CI/CD.

**Desvantagens:**
* Não é flexível na criação de fluxos de trabalho personalizados.
* Processo é difícil de entender para os desenvolvedores com menos experiência.
* Maior "Time-consuming" para resolver problemas de compilação e novas integrações de bibliotecas.
* Não é bom com várias versões da mesma dependência


###***Gradle***
Gradle combina o poder de Ant e Maven.
é um código aberto.
A primeira versão de Gradle foi lançada em 2012. A adoção foi rápida. 
O Google está atualmente usando-o para o sistema operacional Android.
Em vez de XML, Gradle usa a linguagem Groovy. Como resultado, os scripts de construção no Gradle são mais fáceis de escrever e ler. 
Inicialmente estava a usar Ivy para gerir dependências, mas atualmente usa o seu próprio mecanismo de dependência.

**Vantagens:**
* Oferece padronização e, ao mesmo tempo, permanece flexível. 
* Scripts de criação fáceis de ler e gravar.
* Melhor na manipulação de várias versões de dependências.
* Capaz de lidar com várias linguagens de programação e tecnologias.
* Comunidade ativa ajudando a desenvolver a ferramenta.
* O Gradle DSL (Domain-Specific Language, Linguagem Específica de Domínio) simplifica a estrutura de configuração.
* Gradle oferece melhorias de desempenho usando incrementalmente, cache de construção e o Daemon Gradle.

**Desvantagens:**
* A integração do IDE não é tão boa quanto o Maven.

###***Conclusão:***
Entre as ferramentas de construção, o Ant pode ser útil para projetos menores, enquanto o Maven é melhor para garantir que todos os desenvolvedores sigam as mesmas regras. 
Gradle é a ferramenta mais recente que fornece a maior flexibilidade.

