Class Assignment 3 - Part1
--------------------------

##Creating Virtual Machines

Usamos o VirtualBox, um hipervisor gratuito que é executado em muitos sistemas operacionais de host, como Windows, Linux e OSX.

* Uma VM com sistema operacional Linux (Ubuntu 18.04) foi criada.

* Defeni dois adaptadores de rede, o adaptador de rede 1 como Nat e o adaptador de rede 2 como adaptador Host-only.
![img_1.png](img_1.png)

* A VM foi iniciada e o Ubuntu foi instalado.

* Depois de fazer login na VM, fiz a atualização dos packages (*sudo apt update*) e as ferramentas de rede (*sudo apt install net-tools*) foram instaladas e o arquivo de configuração de rede foi editado para ser configurado
  o IP (*sudo nano /etc/netplan/01-netcfg.yaml*) do segundo adaptador como 192.168.56.5 .
![img_2.png](img_2.png)
![img_3.png](img_3.png)
![img_4.png](img_4.png)

* Foram aplicadas as novas alterações (*sudo apt install*).

* O servidor Openssh foi instalado para que possamos usar o ssh para abrir sessões de terminal seguras para a VM de outros hosts.
  A autenticação de senha para ssh também foi habilitada (*sudo apt install openssh-server*).

* Um servidor ftp foi instalado para que possamos usar o protocolo FTP para transferir arquivos de/para a VM de outros hosts.
  O acesso de gravação foi habilitado (*sudo apt install vsftpd*).

* Agora eu posso usar o ssh para me conectar à VM de um terminal no meu computador (host): ssh andreia@192.168.56.5 .
![img_5.png](img_5.png)

* Como o servidor FTP está habilitado na VM, agora podemos usar um aplicativo ftp, como FileZilla, para transferir arquivos para/da VM (*https://filezilla-project.org*).
![img_10.png](img_10.png)

* Fiz a instalação do Git e do Java (*sudo apt install git*, *sudo git*, *sudo apt install openjdk-11-jdk-headless* e *java -version*).
![img_7.png](img_7.png)

**Cloning, Building and Running the Spring Tutorial**
_____
* Instalei e executai o aplicativo Spring Tutorial. Fiz um clone do aplicativo com o seguinte comando (*git clone https://github.com/spring-guides/tut-react-and-spring-data-rest.git*).
![img_8.png](img_8.png)

* Alterei para o diretório da versão basic (*cd tut-react-and-spring-data-rest/basic*).

* Construi e executei o aplicativo (*./mvnw spring-boot:executar*).

* Abri o aplicativo, utilizei o browser no host do computador e o url correto (*http://192.168.56.5:8080/*).
![img_9.png](img_9.png)

## Building and Execute Projects

### Class Assignment 1 - Spring Boot Application with Maven

* Fiz *ssh andreia@192.168.56.5* .

* Fiz *git clone https://Andreia_Alferes@bitbucket.org/andreia_alferes/devops-21-22-atb-1211749.git* .

* Executei 

````
dir
cd devops-21-22-atb-1211749
dir
cd CA1
cd basic
````
![img_13.png](img_13.png)

* Fiz *sudo apt install maven* e *./mvn spring-boot:run* .

* Para projetos da Web como o aplicativo Spring, devo aceder às aplicações da Web a partir do navegador na minha máquina host
  já que a Virtual Machine não tem uma interface gráfica. Assim, no navegador em vez de localhost devo escrever o IP da
  VM: `http://192.168.56.5:8080/`. Agora podemos ver os campos First Name, Last Name, Description, jobYears e email.
![img_14.png](img_14.png)

### Class Assignment 2 - Part 1 - Gradle_basic_demo project

* No terminal, em (*/devops-21-22-atb-1211749/CA2/part1/master*).

* Dei permissão de execução para arquivo gradlew , através de *chmod u+x gradlew*. Fiz *./gradlew build*

* Para que a compilação seja bem-sucedida,tive que comentar a tarefa zipFile e na tarefa runClient alterar localhost para o IP da VM: args "192.168.56.5", "59001".
Para projetos como o do aplicativo de conversação simples, tive de executar a tarefa runServer (*./gradlew runServer*) dentro da VM e da tarefa
runClient (*./gradlew runClient*) na máquina host, já que a VM não tem uma interface gráfica. Executando o cliente no host
máquina, uma janela de conversação apareceu. A execução do cliente noutro terminal de host leva a outra janela de conversação, portanto
cria uma sala de chat.
![img_16.png](img_16.png)

### Class Assignment 2 - Part 2 - Spring Boot Application with Gradle

* No terminal no caminho (*/devops-21-22-atb-1211749/CA2/part2*), ao executar *./gradlew build* a compilação falhou. Para que a construção fosse bem-sucedida, tive de remover a pasta `node` e `node_modules`.
![img_17.png](img_17.png)
````
rm -rf node_modules
rm -rf node
chmod u+x gradlew
chmod u+x webpack.config.js
````

* De seguida executei

````
./gradlew clean
````
![img_18.png](img_18.png)

````
./gradlew build
````
![img_19.png](img_19.png)

````
./gradlew bootRun
````
![img_20.png](img_20.png)

* Abri no browser o`http://192.168.56.5:8080/` no navegador host e podemos ver os campos First Name, Last Name and Description.
![img_21.png](img_21.png)

* Criei o tag ca3-part1 para marcar o fim da parte 1 do CA3.

*Nota*
Todos os procedimentos efetuados ao longo da execução da tarefa foram registadas no ficheiro readme file.




