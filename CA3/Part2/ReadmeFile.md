Class Assigment 3 - Part 2
===================

## 1. Análise, Design e Implementação

O objetivo da Parte 2 desta tarefa é usar o Vagrant para configurar um ambiente virtual para executar o tutorial spring
aplicativo de inicialização, versão "básica" do gradle (desenvolvida em CA2, Part2).

Vagrant é uma ferramenta poderosa que:

* ajuda a automatizar a configuração de uma ou mais VMs, sendo particularmente útil quando temos que lidar com várias VMs.

* permite definir a configuração de uma VM usando um arquivo de configuração simples (Vgrantfile).

* Facilita a reprodução de ambientes: compartilhar uma VM torna-se compartilhar um arquivo de configuração vagrant.

Descarreguei o Vagrantfile de https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/ para ser usado como solução inicial. 
Permite-nos criar e provisionar 2 Virtual Machines:

* web: esta VM é usada para executar o tomcat e o aplicativo básico de inicialização por mola.
* db: esta VM é usada para executar o banco de dados do servidor H2.

* Criei uma pasta com o nome vagrant-project-1 através da linha de comandos onde executei *mkdir vagrant-project-1*. 

![img_23.png](img_23.png)
* Instalei o Vagrant (www.vagrantup.com/downloads.html).

* Na linha de comandos fiz *vagrant -v* e na linha de comandos tive a indicação da versão instalada (vagrant 2.2.19).

* Fiz *vagrant init bento/ubunto-18.04* (compativel com hyperv, parallels, virtualbox, vmware_desktop), posteriormente executei vagrant box add bento/ubuntu 18.04.

* Executei *vagrant box add envimation/ubuntu-xenial* e de seguida *vagrant up*

![img_22.png](img_22.png)
* Criei a pasta CA3/Part2/tut-vag, onde passei as pastas correspondes ao trabalho CA2-Part2

* O Vagrantfile foi copiado para o meu repositório (dentro da pasta deste trabalho) e atualizado para que use a minha
própria versão gradle do aplicativo spring:
```
# Alterei o seguinte comando para clonar o meu próprio repositório!
git clone https://Andreia_Alferes@bitbucket.org/andreia_alferes/devops-21-22-atb-1211749.git
cd devops-21-22-atb-1211749/CA3/Part2/tut-vag
      chmod u+x gradlew
      ./gradlew clean build
      ./gradlew bootWar
      # To deploy the war file to tomcat8 do the following command:
      sudo cp ./build/libs/basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
```

Então fui em https://bitbucket.org/atb/tut-basic-gradle (repositório cedido pelo professor) para ver as mudanças necessárias para que o aplicativo spring
usa no servidor H2  e no db VM (virtual machine). Eu repliquei as alterações na minha própria versão do aplicativo spring, onde efetuei as seguintes alterações:

* Em C:\Devops\devops-21-22-atb-1211749\CA3\Part2\basic\src\main\resources\application properties:
```
server.servlet.context-path=/basic-0.0.1-SNAPSHOT
spring.data.rest.base-path=/api
#spring.datasource.url=jdbc:h2:mem:jpadb
# In the following settings the h2 file is created in /home/vagrant folder
spring.datasource.url=jdbc:h2:tcp://192.168.56.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
# So that spring will no drop de database on every execution.
spring.jpa.hibernate.ddl-auto=update
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
spring.h2.console.settings.web-allow-others=true
```

* Em C:\Devops\devops-21-22-atb-1211749\CA3\Part2\basic\src\main\resources\templates\index:
```
<!DOCTYPE html>
<html xmlns:th="https://www.thymeleaf.org">
<head lang="en">
    <meta charset="UTF-8"/>
    <title>ReactJS + Spring Data REST</title>
    <link rel="stylesheet" href="main.css" />
</head>
<body>

<div id="react"></div>

<script src="built/bundle.js"></script>

</body>
</html>
```

* Em C:\Devops\devops-21-22-atb-1211749\CA3\Part2\basic\src\main\js\app.js:
```
'use strict';

// tag::vars[]
const React = require('react'); // <1>
const ReactDOM = require('react-dom'); // <2>
const client = require('./client'); // <3>
// end::vars[]

componentDidMount() { // <2>
		client({method: 'GET', path: '/basic-0.0.1-SNAPSHOT/api/employees'}).done(response => {
			this.setState({employees: response.entity._embedded.employees});
		});
	}
```

* Em C:\Devops\devops-21-22-atb-1211749\CA3\Part2\basic\src\main\java\com\greglturnquist\payroll
```
Foi criada uma nova classe ServletInitializer.java onde foi adicionado:

package com.greglturnquist.payroll;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ReactAndSpringDataRestApplication.class);
    }
}
```
* Em C:\Devops\devops-21-22-atb-1211749\CA3\Part2\basic\.gitignore
````
Foi acrescentado à pasta .gitignore o que constava no tutorial tut-basic-gradle

# Created by https://www.gitignore.io/api/gradle
# Edit at https://www.gitignore.io/?templates=gradle

### Gradle ###
.gradle
build/

# Ignore Gradle GUI config
gradle-app.setting

# Avoid ignoring Gradle wrapper jar file (.jar files are usually ignored)
!gradle-wrapper.jar

# Cache of project
.gradletasknamecache

# # Work around https://youtrack.jetbrains.com/issue/IDEA-116898
# gradle/wrapper/gradle-wrapper.properties

### Gradle Patch ###
**/build/

# End of https://www.gitignore.io/api/gradle

*.iml
.idea
target
bower_components
node_modules
node
*.log
asciidoctor.css
README.html
built
dump.rdb
package-lock.json
# Created by https://www.toptal.com/developers/gitignore/api/eclipse
# Edit at https://www.toptal.com/developers/gitignore?templates=eclipse

### Eclipse ###
.metadata
bin/
tmp/
*.tmp
*.bak
*.swp
*~.nib
local.properties
.settings/
.loadpath
.recommenders

# External tool builders
.externalToolBuilders/

# Locally stored "Eclipse launch configurations"
*.launch

# PyDev specific (Python IDE for Eclipse)
*.pydevproject

# CDT-specific (C/C++ Development Tooling)
.cproject

# CDT- autotools
.autotools

# Java annotation processor (APT)
.factorypath

# PDT-specific (PHP Development Tools)
.buildpath

# sbteclipse plugin
.target

# Tern plugin
.tern-project

# TeXlipse plugin
.texlipse

# STS (Spring Tool Suite)
.springBeans

# Code Recommenders
.recommenders/

# Annotation Processing
.apt_generated/
.apt_generated_test/

# Scala IDE specific (Scala & Java development for Eclipse)
.cache-main
.scala_dependencies
.worksheet

# Uncomment this line if you wish to ignore the project description file.
# Typically, this file would be tracked if it contains build/dependency configurations:
.project
.classpath

### Eclipse Patch ###
# Spring Boot Tooling
.sts4-cache/

# End of https://www.toptal.com/developers/gitignore/api/eclipse
````
* Em C:\Devops\devops-21-22-atb-1211749\CA3\Part2\basic\package.json
````
 "scripts": {
    "watch": "webpack --watch -d",
    "webpack": "webpack"
````
* Em C:\Devops\devops-21-22-atb-1211749\CA3\Part2\basic\settings.gradle
````
rootProject.name = 'basic'
````

* Como o trabalho desenvolvido no CA2-part2 tinha sido efetuado utilizando a versão 11 do java e uma vez que o vagrantFile cedido pelo professor foi feito com base no java versão 8, procedi às seguintes alterações:
````
No vagrantFile:
 sudo apt-get install openjdk-11-jdk-headless -y

No build.gradle:
sourceCompatibility = '11'
````
* Na pasta com o Vagrantfile, executei `vagrant up` para criar duas VMs vagrant ("db" e "web"). A primeira vez que é iniciado
leva mais tempo porque ambas as máquinas serão provisionadas com vários pacotes de software. 

![img_1.png](img_1.png)

* Depois de inicializada, executei `vagrant status`.

![img.png](img.png)

* Para aceder à VM da web executei `vagrant ssh web`.

* No host, abri o browser com as seguintes opções:

* http://localhost:8080/

![img_2.png](img_2.png)

* http://localhost:8080/basic-0.0.1-SNAPSHOT/

![img_3.png](img_3.png)

* Após várias tentativas e sem conseguir determinar a origem do erro que me estava a impedir abrir no browser e finalizar a tarefa decidi eleminar a pasta CA3 part2 e criei uma nova (C:\Devops\devops-21-22-atb-1211749\CA3\Part2) onde coloquei novamente o vagrantFile e fiz uma nova pasta chamada basic com as pastas referentes ao trabalho CA2 part2.
Para as operações de destruição como de criação de nova pasta executei:
````
git add .
git commit -m "delete Part2 to iniciate again"
git push origin master

git add .
git commit -m "Create a new folder CA3 Part2"
git push origin master
````
* Tal como anteriormente mantive o bento/ubunto-18.04.

* Procedi às seguintes alterações:
```
# Alterei o seguinte comando para clonar o meu próprio repositório!
git clone https://Andreia_Alferes@bitbucket.org/andreia_alferes/devops-21-22-atb-1211749.git
cd devops-21-22-atb-1211749/CA3/Part2/basic
chmod u+x gradlew
./gradlew clean build
./gradlew bootWar
# To deploy the war file to tomcat8 do the following command:
sudo cp ./build/libs/basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
```

* Em C:\Devops\devops-21-22-atb-1211749\CA3\Part2\basic\src\main\resources\application properties:
```
server.servlet.context-path=/basic-0.0.1-SNAPSHOT
spring.data.rest.base-path=/api
#spring.datasource.url=jdbc:h2:mem:jpadb
# In the following settings the h2 file is created in /home/vagrant folder
spring.datasource.url=jdbc:h2:tcp://192.168.56.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
# So that spring will no drop de database on every execution.
spring.jpa.hibernate.ddl-auto=update
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
spring.h2.console.settings.web-allow-others=true
```

* Em C:\Devops\devops-21-22-atb-1211749\CA3\Part2\basic\src\main\resources\templates\index:
```
<!DOCTYPE html>
<html xmlns:th="https://www.thymeleaf.org">
<head lang="en">
    <meta charset="UTF-8"/>
    <title>ReactJS + Spring Data REST</title>
    <link rel="stylesheet" href="main.css" />
</head>
<body>

<div id="react"></div>

<script src="built/bundle.js"></script>

</body>
</html>
```

* Em C:\Devops\devops-21-22-atb-1211749\CA3\Part2\basic\src\main\js\app.js:
```
'use strict';

// tag::vars[]
const React = require('react'); // <1>
const ReactDOM = require('react-dom'); // <2>
const client = require('./client'); // <3>
// end::vars[]

componentDidMount() { // <2>
		client({method: 'GET', path: '/basic-0.0.1-SNAPSHOT/api/employees'}).done(response => {
			this.setState({employees: response.entity._embedded.employees});
		});
	}
```

* Em C:\Devops\devops-21-22-atb-1211749\CA3\Part2\basic\src\main\java\com\greglturnquist\payroll
```
Foi criada uma nova classe ServletInitializer.java onde foi adicionado:

package com.greglturnquist.payroll;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ReactAndSpringDataRestApplication.class);
    }
}
```
* Em C:\Devops\devops-21-22-atb-1211749\CA3\Part2\basic\.gitignore
````
Foi acrescentado à pasta .gitignore o que constava no tutorial tut-basic-gradle

# Created by https://www.gitignore.io/api/gradle
# Edit at https://www.gitignore.io/?templates=gradle

### Gradle ###
.gradle
build/

# Ignore Gradle GUI config
gradle-app.setting

# Avoid ignoring Gradle wrapper jar file (.jar files are usually ignored)
!gradle-wrapper.jar

# Cache of project
.gradletasknamecache

# # Work around https://youtrack.jetbrains.com/issue/IDEA-116898
# gradle/wrapper/gradle-wrapper.properties

### Gradle Patch ###
**/build/

# End of https://www.gitignore.io/api/gradle

*.iml
.idea
target
bower_components
node_modules
node
*.log
asciidoctor.css
README.html
built
dump.rdb
package-lock.json
# Created by https://www.toptal.com/developers/gitignore/api/eclipse
# Edit at https://www.toptal.com/developers/gitignore?templates=eclipse

### Eclipse ###
.metadata
bin/
tmp/
*.tmp
*.bak
*.swp
*~.nib
local.properties
.settings/
.loadpath
.recommenders

# External tool builders
.externalToolBuilders/

# Locally stored "Eclipse launch configurations"
*.launch

# PyDev specific (Python IDE for Eclipse)
*.pydevproject

# CDT-specific (C/C++ Development Tooling)
.cproject

# CDT- autotools
.autotools

# Java annotation processor (APT)
.factorypath

# PDT-specific (PHP Development Tools)
.buildpath

# sbteclipse plugin
.target

# Tern plugin
.tern-project

# TeXlipse plugin
.texlipse

# STS (Spring Tool Suite)
.springBeans

# Code Recommenders
.recommenders/

# Annotation Processing
.apt_generated/
.apt_generated_test/

# Scala IDE specific (Scala & Java development for Eclipse)
.cache-main
.scala_dependencies
.worksheet

# Uncomment this line if you wish to ignore the project description file.
# Typically, this file would be tracked if it contains build/dependency configurations:
.project
.classpath

### Eclipse Patch ###
# Spring Boot Tooling
.sts4-cache/

# End of https://www.toptal.com/developers/gitignore/api/eclipse
````
* Em C:\Devops\devops-21-22-atb-1211749\CA3\Part2\basic\package.json
````
 "scripts": {
    "watch": "webpack --watch -d",
    "webpack": "webpack"
````
* Em C:\Devops\devops-21-22-atb-1211749\CA3\Part2\basic\settings.gradle
````
rootProject.name = 'basic'
````
* Tal como tinha feito anteriormente alterei a versão do java para 11:
````
No vagrantFile:
 sudo apt-get install openjdk-11-jdk-headless -y

No build.gradle:
sourceCompatibility = '11'
````
* Na pasta com o Vagrantfile, voltei a executar o `vagrant up` para criar duas VMs vagrant ("db" e "web"). A primeira vez que é iniciado
  leva mais tempo porque ambas as máquinas serão provisionadas com vários pacotes de software. 

![img_4.png](img_4.png)
* Depois de inicializada, executei `vagrant status`.

![img.png](img.png)
* No host, abri o browser com as seguintes opções:

* http://localhost:8080/
  
![img_2.png](img_2.png)

* http://localhost:8080/basic-0.0.1-SNAPSHOT/
 
![img_5.png](img_5.png)
* http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console

![img_6.png](img_6.png)
* Para a conexão da string, usei: jdbc:h2:tcp://192.168.56.11:9092/./jpadb

![img_7.png](img_7.png)

![img_8.png](img_8.png)
* Introduzi insert into employees (id, description, first_name, last_name)values(99,'developer','Andreia','Alferes') fiz "connect" e no browser coloquei 'http://localhost:8080/basic-0.0.1-SNAPSHOT/', onde obtive:

![img_9.png](img_9.png)

## 2. Análise e implementação de uma alternativa

O Vagrant usa o VirtualBox como hipervisor padrão (ele também vem pronto para uso com suporte para Hyper-V e Docker), mas
também pode usar outros 'providers' como VMware. Os 'providers' VMware são bem suportados e geralmente considerados mais estáveis
e desempenho do que o VirtualBox.

### VirtualBox vs VMware

* O VirtualBox é um produto de virtualização gratuito e multiplataforma.

* Um 'provider' oficial do VMware Fusion e VMware Workstation foi desenvolvido para o Vagrant. Este 'provider' permite que o Vagrant
  potencialize máquinas baseadas em VMware e aproveite a estabilidade e o desempenho aprimorados que o software VMware oferece,
  mas requer a compra de uma licença para operar.

* De acordo com a HashiCorp, usar um novo 'provider' não deve exigir configuração específica do 'provider' para funcionar, se
possível. Quando um usuário instala o vmware_fusion, idealmente ele deve ser capaz de `vagrant up --provider=vmware_fusion` e
tê-lo apenas trabalhar.

* Os 'providers' são distribuídos como plugins Vagrant e as box Vagrant são todas específicas do 'provider'. Uma box para VirtualBox é
incompatível com o 'provider' VMware Fusion ou qualquer outro 'provider'.

* Se tivermos um ambiente multi-máquina, podemos trazer uma máquina apoiada pelo VirtualBox e outra apoiada por
VMware Fusion, por exemplo, mas não podemos fazer backup da mesma máquina com VirtualBox e VMware Fusion. Podemos usar
`config.vm.provider` sem configuração no topo do nosso Vagrantfile para definir a ordem dos 'providers' que preferimos
suportar.

### Implementação da alternativa com VMware

* Tal como já foi referido anteriormente o VirtualBox é o 'provider' padrão do Vagrant. No entanto, o Vagrant suporta outros 'hypervisors'.
  Podemos alterar o provedor padrão do Vagrant definindo a variável de ambiente VAGRANT_DEFAULT_PROVIDER.
  Para implementar a solução alternativa, usaremos o **VMWare Workstation Player**.

* A instalação do 'provider' Vagrant VMware, foi efetuado em duas etapas:

*1.* Fiz a instalação do VMware Workstation 16 player, através do site 'https://www.vmware.com/products/workstation-player/workstation-player-evaluation.html'

![img_11.png](img_11.png)

*2.* Instalei o plugin do 'provider' Vagrant VMware, onde executei o comando:
````
vagrant plugin install vagrant-vmware-desktop
````

![img_12.png](img_12.png)

* Criei a pasta CA3/Part2/Part2-Alternativa_VMWARE, onde passei a vagrantFile referente à task anterior e criei uma pasta basic onde passei as pastas referentes ao CA2 Part2.

* Foi copiado para o meu repositório e atualizado para que use a minha  própria versão gradle do aplicativo spring.
  Para além das alterações que já tinham sido efetuadas anteriormente (CA3-Part2), foi necessário alterar:
* Em C:\Devops\devops-21-22-atb-1211749\CA3\Part2-Alternativa_VMWARE\vagrantfile:
````
 Configurations specific to the database VM
    config.vm.define "db" do |db|
    db.vm.provider "vmware_desktop"
    db.vm.box = "bento/ubuntu-18.04"
    db.vm.hostname = "db"
    db.vm.network "private_network", ip: "192.168.58.11"
    
  Configurations specific to the webserver VM
    config.vm.define "web" do |web|
    web.vm.box = "bento/ubuntu-18.04"
    web.vm.hostname = "web"
    web.vm.network "private_network", ip: "192.168.58.10"

  We set more ram memmory for this VM
    web.vm.provider "vmware_desktop" do |v|
    v.memory = 1024
  
  Change the following command to clone your own repository! 
    cd devops-21-22-atb-1211749/CA3/Part2-Alternativa_VMWARE/basic
````
* Em C:\Devops\devops-21-22-atb-1211749\CA3\Part2-Alternativa_VMWARE\basic\src\main\resources\application properties:
````
spring.datasource.url=jdbc:h2:tcp://192.168.58.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
````
* Na pasta com o Vagrantfile, voltei a executar o `vagrant up --provider vmware_desktop` para criar duas VMs vagrant ("db" e "web"). A primeira vez que é iniciado
  leva mais tempo porque ambas as máquinas serão provisionadas com vários pacotes de software.

![img_13.png](img_13.png)
* No host, abri o browser com as seguintes opções:
* http://localhost:8080/

![img_14.png](img_14.png)
* http://localhost:8080/basic-0.0.1-SNAPSHOT/

![img_15.png](img_15.png)
* http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console

![img_16.png](img_16.png)
* Para a conexão da string, usei: jdbc:h2:tcp://192.168.56.11:9092/./jpadb

![img_17.png](img_17.png)

![img_18.png](img_18.png)

![img_19.png](img_19.png)

![img_20.png](img_20.png)

* Introduzi insert into employees (id, description, first_name, last_name)values(99,'developer','Andreia','Alferes') fiz "connect" e no browser coloquei 'http://localhost:8080/basic-0.0.1-SNAPSHOT/', onde obtive:
![img_21.png](img_21.png)

* Para terminar a tarefa criei o tag ca3-part2 para marcar o fim da parte 2 do CA3. Para tal executei:
```` bash
   git status
   git add .
   git commit -m "End the assignment CA3 Part2  and tag ca3-part2"
   git push origin master
   git tag -a CA3-Part2 -m "End the assignment CA3 Part2  and tag ca3-part2"
   git push origin ca3-part2
````
**Nota:**
Todos os procedimentos efetuados ao longo da execução da tarefa foram registadas no ficheiro readme file..















