/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

    private @Id
    @GeneratedValue
    Long id; // <2>
    private String firstName;
    private String lastName;
    private String description;
    private int jobYears;
    private String email;


    public Employee(String firstName, String lastName, String description, int jobYears, String email) {
        this.firstName = validateStringFirstName(firstName);
        this.lastName = validateStringLastName(lastName);
        this.description = validateStringDescription(description);
        this.jobYears = validateJobYears(jobYears);
        this.email = validateEmail (email);

    }

    public Employee() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equals(id, employee.id) &&
                Objects.equals(firstName, employee.firstName) &&
                Objects.equals(lastName, employee.lastName) &&
                Objects.equals(description, employee.description) &&
                Objects.equals(jobYears, employee.jobYears)&&
                Objects.equals(email, employee.email);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, firstName, lastName, description, jobYears, email);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getJobYears() { return jobYears;}

    public void setJobYears(int jobYears) { this.jobYears = jobYears;}

    public String getEmail() { return email;}

    public void setEmail(int jobYears) { this.email = email;}

    //validation First name
    /**
     * @param firstName
     */
    protected String validateStringFirstName(String firstName) {
        if (firstName == null) {
            throw new IllegalArgumentException("incorrect name");
        }

        firstName = firstName.trim();

        if (firstName.length() == 0) {
            throw new IllegalArgumentException("is empty");
        }

        return firstName;

    }

    //validation Last name
    /**
     * @param lastName
     */
    protected String validateStringLastName(String lastName) {
        if (lastName == null) {
            throw new IllegalArgumentException("incorrect name");
        }

        lastName = lastName.trim();

        if (lastName.length() == 0) {
            throw new IllegalArgumentException(" is empty");
        }

        return lastName;
    }
    //validation Description
    /**
     * @param description
     */
    protected String validateStringDescription(String description) {
        if (description == null) {
            throw new IllegalArgumentException( " is incorrect");
        }

        description = description;

        if (description.length() == 0) {
            throw new IllegalArgumentException(" is empty");
        }

        return description;
    }
    //validate id
    /**
     * @param id
     */

    protected long validateLongId(long id) {
        if (id > 0 ) {
            this.id = id;
        }
        else {
            throw new IllegalArgumentException("Invalid number");
        }
        return id;
    }

    //validate jobYears
    /**
     * @param jobYears
     */

    protected int validateJobYears(int jobYears) {
        if (jobYears > 0 ) {
            this.jobYears= jobYears;
        }
        else {
            throw new IllegalArgumentException("Invalid number");
        }
        return jobYears;
    }

    //validation Email
    /**
     * @param  email
     */
    protected String validateEmail(String email) {
        if (email == null) {
            throw new IllegalArgumentException("Email cannot be null");
        }
        if (email.length() == 0) {
            throw new IllegalArgumentException("Email cannot be empty");
        }

        final Pattern emailPattern = Pattern.compile(
                "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
                Pattern.CASE_INSENSITIVE);
        final Matcher emailMatcher = emailPattern.matcher(email);
        if (!emailMatcher.matches()) {
            throw new IllegalArgumentException("Email format is invalid");
        }
        return email;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", description='" + description + '\'' +
                ", jobYears=" + jobYears + '\'' + ", email=" + email;
    }
}
// end::code[]
