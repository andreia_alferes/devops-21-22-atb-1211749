package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class EmployeeTest {

    @Test
    void validateStringFirstNameNull() {
        String firstName = null;
        Employee employee1 = new Employee();
        assertThrows(IllegalArgumentException.class,
                () -> {
                    employee1.validateStringFirstName(firstName);
                });
    }
    @Test
    void validateStringFirstNameEmpty() {
        String firstName = "";
        Employee employee1 = new Employee();
        assertThrows(IllegalArgumentException.class,
                () -> {
                    employee1.validateStringFirstName(firstName);
                });
    }
    @Test
    void validateStringFirstName() {

        Employee employee1 = new Employee("Andreia","Alferes", "developer",9,"andreia@isep.ipp.pt");
        String firstname = "Andreia";

        String expected = "Andreia";
        String result = employee1.validateStringFirstName(firstname);

        assertEquals(expected, result);
    }

    @Test
    void validateStringLastNameNull() {
        String lastName = null;
        Employee employee1 = new Employee();
        assertThrows(IllegalArgumentException.class,
                () -> {
                    employee1.validateStringLastName(lastName);
                });
    }
    @Test
    void validateStringLastNameEmpty() {
        String lastName = "";
        Employee employee1 = new Employee();
        assertThrows(IllegalArgumentException.class,
                () -> {
                    employee1.validateStringLastName(lastName);
                });
    }
    @Test
    void validateStringLastName() {

        Employee employee1 = new Employee("Andreia","Alferes", "developer",9,"andreia@isep.ipp.pt");
        String lastName = "Alferes";

        String expected = "Alferes";
        String result = employee1.validateStringLastName(lastName);

        assertEquals(expected, result);
    }
    @Test
    void validateStringDescriptionNull() {
        String description = null;
        Employee employee1 = new Employee();
        assertThrows(IllegalArgumentException.class,
                () -> {
                    employee1.validateStringDescription(description);
                });
    }
    @Test
    void validateStringDescriptionEmpty() {
        String description = "";
        Employee employee1 = new Employee();
        assertThrows(IllegalArgumentException.class,
                () -> {
                    employee1.validateStringDescription(description);
                });
    }
    @Test
    void validateStringDescription() {

        Employee employee1 = new Employee("Andreia","Alferes", "developer",9, "andreia@isep.ipp.pt");
        String description = "developer";

        String expected = "developer";
        String result = employee1.validateStringDescription(description);

        assertEquals(expected, result);
    }
    @Test
    void validateJobYearsNull() {
        int jobYears = 0;
        Employee employee1 = new Employee();
        assertThrows(IllegalArgumentException.class,
                () -> {
                    employee1.validateJobYears(jobYears);
                });
    }
    @Test
    void validateJobYearsNegative() {
        int jobYears = -1;
        Employee employee1 = new Employee();
        assertThrows(IllegalArgumentException.class,
                () -> {
                    employee1.validateJobYears(jobYears);
                });
    }
    @Test
    void validateJobYears() {

        Employee employee1 = new Employee("Andreia","Alferes", "developer",9, "andreia@isep.ipp.pt");
        int jobYears = 9;

        int expected = 9;
        int result = employee1.validateJobYears(jobYears);

        assertEquals(expected, result);
    }
    @Test
    void validateStringEmailNull() {
        String email = null;
        Employee employee1 = new Employee();
        assertThrows(IllegalArgumentException.class,
                () -> {
                    employee1.validateEmail(email);
                });
    }
    @Test
    void validateStringEmailEmpty() {
        String email = "";
        Employee employee1 = new Employee();
        assertThrows(IllegalArgumentException.class,
                () -> {
                    employee1.validateEmail(email);
                });
    }
    @Test
    void validateStringEmailIsNotValid() {
        String email = "andreia_isep.ipp.pt";
        Employee employee1 = new Employee();
        assertThrows(IllegalArgumentException.class,
                () -> {
                    employee1.validateEmail(email);
                });


    }
}




