#**Class Assignment PART1**


##**CA1 - Version Control with Git**

* Repositório Bitbucket - https://bitbucket.org/andreia_alferes/devops-21-22-atb-1211749/src/master/


**1.** Criei a pasta CA1 dentro da pasta devops-21-22-atb-1211749, para tal foi utilizado o comando **mkdir** CA1.

**2.** Copiei o conteúdo da pasta C:\Devops\tut-react-and-spring-data-rest à exepção do **.git** para a pasta CA1.
Utilizei o comando **git mv "nome da pasta a mover"** "CA1".

**3.** Para iniciar a resolução do exercício foram criados diferentes issues relativos à 1ª parte, no repositório (https://bitbucket.org/andreia_alferes/devops-21-22-atb-1211749/src/master/). 
No separador issues criei todos os issues relativos à 1ª parte  **"Create issue"** dei um titulo, referi como task em Kind e assignei a prioridade a cada uma delas.

**4.** No terminal Git Bash foram usados os seguintes comandos:
* git status
* git add .
* git commit -a -m "Create new file CA1"
* git push

**5.** Ao fazer git status tive a indicação de que a 1ª pasta estava "untracked" para resolver fiz 
* git add .. /tutorial/
* git commit -m "Restore tutorial file"
* git push 

**6.** Para criar o tag v1.1.0 foi feito:
* git tag
* git tag -a v1.1.0
* git push origin v1.1.0

Ao fazer o git push origin v1.1.0 verifiquei que o tag não ficou assignado ao commit que pretendia "Create new file CA1" tendo ficado assignado ao commit "Restore tutorial file", para resolver esta situaçáo utilizei os seguintes copmandos:
* git tag v1.1.0 a063b33
* git push origin --tags

Após a alteração do tag para o commit pretendido fiquei com a indicação de que o HEAD tinha sido alterado de master para 08e5e69.
Para reverter esta situação utilizei o comando:
* git rebase Head master

Posteriormente verifiquei que ficaram criados 2 tags, um associado ao commit "Create new file CA1" e outro a "Restore tutorial file" de forma a corrigir esta questão ficando apenas o tag no commit "Create new file CA1" utilizei os seguintes comandos:
* git tag
* git push origin : refs/tags/v1.1.0 

**7.** Criei o ficheiro Readme 
* echo "Hello World" >> Readme.md
* git status
* git add Readme.md
* git commit -m "Add Readme file"
* git push

**8.** Verifiquei que o tag que ficou assignado não estava de acordo com o que foi dado no exercício a realizar dessa forma procedi à sua correção, para tal utilizei os sewguintes comandos:
* git tag
* v.1.1.0 (era o que estava criado e tinha de ser corrigido para v1.1.0)

* git tag v.1.1.0 v1.1.0
* git tag -d v.1.1.0
* git push origin v1.1.0 : v1.1.0

* git status
* git add .
* git commit -m "Resolved issue #2 tag the initial version as v1.1.0"
* git push

**9.** Criação do "field Job Years" - para tal foram efetuadas alterações no código nomeadamente nas classes:
**Databaseloader** 
(da linha 38 à 43 foram acrescentado aos empregados a opção de job years)

**Employee** 
(foi colocado o atributo jobYears - linha 36, colocado no construtor - linha 43 e como argumento, no equals - linha 59, foi feito o get - linha 100 e o set -linha 102, tambem foi acrescentado a public String toString() - linha 194)

**app.js**
(acrescentado na linha 44 -<th>jobYears</th> e na linha 62 - <td>{this.props.employee.jobYears}</td>)

* git status
* git add .
* git commit -m "Add a new feature to add a new field to the aplication (jobYears), Resolved #3"
* git push 

Para verificar se as alterações foram efetuadas fiz o seguinte: 
* cd CA1
* cd basic
* ./mvnw spring-boot:run (no terminal)
* http://localhost:8080/, surge com a atualização efetuada (imagem abaixo)
![img.png](img.png)

**10.** Com as alterações efetuadas nas diferentes classes foram efetuados os comandos:
* git status
* git add . 
* git commit -m "Add changes to class Employee"

**11.** Efetuadas validações para cada um dos atributos que constituem os empregados (first name, last name, description, job years) considerando o cenário null, empty e valores negativos (jobyears)

**First name**
![img_1.png](img_1.png)

* git status
* git add .
* git commit -m "Add validateStringFirstName, see #4"
* git push 

**Last name**
![img_2.png](img_2.png)

* git status
* git add .
* git commit -m "Add validateStringLastName, see #4"
* git push

**Description**
![img_3.png](img_3.png)

* git status
* git add .
* git commit -m "Add validateStringDescription, see #4"
* git push

**Id**
![img_4.png](img_4.png)

* git status
* git add .
* git commit -m "Add validateId, see #4"
* git push

**JobYears**
![img_5.png](img_5.png)

* git status
* git add .
* git commit -m "Add validateJobYears, see #4"
* git push

**12.** Testes referentes à criação de empregados
**FirstName (null, empty)**
![img_7.png](img_7.png)

* git status
* git add .
* git commit -m "Add validate firstName test (null, empty) method, see #4"
* git push

**FirstName (correct case)**
![img_8.png](img_8.png)
* git status
* git add .
* git commit -m "Add validate firstName test (correct), see #4"
* git push

**LastName(null, empty and correct case)**
![img_10.png](img_10.png)
* git status
* git add .
* git commit -m "Add tests to validate lastName (null, empty and correct scenario), see #4"
* git push

**Description(null, empty and correct case)**
![img_11.png](img_11.png)
* git status
* git add .
* git commit -m "Add tests to validate description (null, empty and correct scenario), see #4"
* git push

**jobYears(valores <0 e correct case)**
![img_12.png](img_12.png)
* git status
* git add .
* git commit -m "Add tests to validate jobYears (null, empty and correct scenario), see #4"
* git push

Nota: No fim foi feita uma simulação onde na classe DataBaseLoader foi deixado o FirstName "", ao correr o, ./mvnw spring-boot:run (no terminal) e de seguida abrir http://localhost:8080/, surge erro.

* git status
* git add .
* git commit -m "Resolved #4 and debug server"
* git push

Foram feitas pequenas correções para poder fazer os seguintes commits:
* git status 
* git add . 
* git commit -m "Resolved #5 refered in the previous commit"
* git push

* git status
* git add . 
* git commit-m "Class Employee is completed and tested"
* git push

**13.** Acrescentado novo tag
* git tag
* git tag -a v1.2.0 -m "Add version 1.2.0" 654f59a 
* git tag
* git push origin v1.2.0

* git status
* git add . 
* git commit -m "Resolved #6"
* git push 

Feitas correções estruturais no código
* git status
* git add .
* git commit -m "End partI"
* git push

**14.** Adicionar o tag ca1-part1
* git tag
* git tag -a ca1-part1 -m "add version ca1-part1" 50274c6
* git tag
* git push origin ca1-part1

* git status
* git add .
* git commit -m "Resolved # and end part1"
* git push

Para dar inicio à segunda parte do exercicio foram criados vários issues:
![img_28.png](img_28.png)

**15.** Adicionar um branch email-field:
* git branch email-field
* git branch (Head -> Master)
* git checkout email-field
* git branch (Head -> email-field)
* git status
* git add .
* git commit -a -m "Create branch email-field, Resolved #8"
* git push --set-upstream origin email-field
* git pull

**16.** Adicionar um novo campo ao projeto, designado de email e para tal foram feitas alterações nas seguintes classes:
                                   
**Databaseloader**
(da linha 38 à 42 foi acrescentado aos empregados a opção de email)

**Employee**
(foi colocado o atributo jobYears - linha 37, colocado no construtor - linha 46 e como argumento, no equals - linha 59, foi feito o get e o set -da linha 108 e 110, também foi acrescentado a public String toString() - linha 194)

**app.js**
(acrescentado campo email na linha 45 e 64

Após estas alterações foi feito:
* git status
* git add .
* git commit -a -m "Create support email-field, Resolved #9"
* git push --set-upstream origin email-field
* git pull

Para verificar se as alterações foram efetuadas fiz o seguinte:
* cd CA1
* cd basic
* ./mvnw spring-boot:run (no terminal)
* http://localhost:8080/, surge com a atualização efetuada (imagem abaixo)
![img_25.png](img_25.png)

**17.** Adicionar testes para validar o cenário empty, null para o campo email
![img_30.png](img_30.png)

* git status
* git add .
* git commit -m "Add tests for email fiel, Resolved #10"
* git push --set-upstream origin email-field

**18.** Merge with master 

* git branch (Head -> email-field)
* git checkout master
* git merge email-field
* git branch (Head -> Master)
* git commit -m "Merge with new feature and tag v1.3.0, Resolved #11"
* git push origin master 
* git log--oneline--decorate--graph--all ![img_26.png](img_26.png)

**19.** Criar tag v1.3.0
* git tag (ca1-part1, v1.1.0, v1.2.0)
* git tag -a v1.3.0 -m "new tag" 8a3b1a4 (hash do commit)
* git tag (ca1-part1, v1.1.0, v1.2.0, v1.3.0)
* git push origin v1.3.0

**20.** Adicionar novo branch fix-invalid-email
* git branch fix-invalid-email
* git branch (Head -> Master)
* git checkout fix-invalid-email
* git branch (Head -> fix-invalid-email)
* git status
* git add .
* git commit -m "Create new branch fix-invalid-email, Resolved #12"
* git push --set-upstream origin fix-invalid-email

**21.** Adicionar validações que contemplam o simbolo "@" que faz parte do email 
![img_31.png](img_31.png)

* git status
* git add .
* git commit -m "Add validations to accept "@" in email field, Resolved #13"
* git push --set-upstream origin fix-invalid-email

Para verificar se as alterações foram efetuadas fiz o seguinte:
* cd CA1
* cd basic
* ./mvnw spring-boot:run (no terminal)
* http://localhost:8080/, surge com a atualização efetuada (imagem abaixo)
  ![img_25.png](img_25.png)

**22.** Adicionar testes relativos à validação anterior
![img_32.png](img_32.png)
* git status
* git add .
* git commit -m "Add validation to accept "@" in email field and tests, Resolved #13 and #14"
* * git push --set-upstream origin fix-invalid-email

**23.** Merge com o master e criar o tag v1.3.1
* git branch (Head -> fix-invalid-email)
* git checkout master
* git merge fix-invalid-email
* git branch (Head -> master)
* git commit -m "Merge into master, Resolved #15"
* git push origin master
* git log--oneline--decorate--graph--all

* git tag (ca1-part1,v1.1.0,v1.2.0,v1.3.0)
* git tag -a v1.3.1 -m "New version" 1e09600 (hash do commit)
* git tag (ca1-part1,v1.1.0,v1.2.0,v1.3.0,v1.3.1)
* git push origin v1.3.1

* git status
* git add .
* git commit -m "Add new tag v1.3.1, Resolved #16"
* git push

**24.** Terminar a tarefa tag ca1
* git status
* git add .
* git commit -m "End the assignment, part2"
* git push

* git tag (ca1-part1,v1.1.0,v1.2.0,v1.3.0,v1.3.1)
* git tag -a ca1 -m "End assignment" d8cbe77 (hash do commit)
* git tag (ca1,ca1-part1,v1.1.0,v1.2.0,v1.3.0,v1.3.1)
* git push origin ca1

* git status
* git add .
* git commit -m "End assignment part2, Resolved #17"
* git push


##**Versão alternativa escolhida - Mercurial**

**1.** 
* Criei pasta Devops_Mercurial (C:\Devops_Mercurial)
* Dentro da pasta anterior foi criada Ca1_mercurial com o comando mkdir Ca1_mercurial (C:\Devops_Mercurial\CA1_Mercurial)
*Foram copiadas para a pasta anterior todas as pastas do tutorial React.JS and Spring data rest Aplication, sem o .git 

* Foi feito o registo no helixteamhub.cloud e criado o repositório remoto - https://helixteamhub.cloud/crimson-shade-24/projects/devops-21-22-atb-1211749/repositories
* Criei projeto devops-21-22-ATB-1211749
* Criei repositório - NewRepository - Mercurial - short name devops - 21-22-atb-1211749-Mercurial

Iniciei repositório (na linha de comandos do admnistrador):
* cd \Devops_Mercurial\
* echo "#Hello world" >> Readme.md
* hg init (foi criado o .hg)
  ![img_13.png](img_13.png)
* hg add Readme.md
* hg commit -m "Initial commit"
* hg push https://1211749isepipppt@helixteamhub.cloud/crimson-shade-24/projects/devops-21-22-atb-1211749/repositories/mercurial/devops-21-22-atb-1211749-Mercurial

* hg status 
* hg add
* hg commit -m "Resolved #1 create folder ca1"
* hg push https://1211749isepipppt@helixteamhub.cloud/crimson-shade-24/projects/devops-21-22-atb-1211749/repositories/mercurial/devops-21-22-atb-1211749-Mercurial

**2.** Criei uma lista de issues relativas à 1ª parte do exercicio (ca1-part1)
![img_14.png](img_14.png)

**3.** Criação do tag v1.1.0
* hg tag v1.1.0
* hg tags
* hg status
* hg add
* hg commit - m "Resolved #2 Add tag v1.1.0"
* hg push https://1211749isepipppt@helixteamhub.cloud/crimson-shade-24/projects/devops-21-22-atb-1211749/repositories/mercurial/devops-21-22-atb-1211749-Mercurial

**4.** Criação do "field Job Years" - para tal foram efetuadas alterações no código nomeadamente nas classes:
*Databaseloader
(da linha 38 à 43 foram acrescentado aos empregados a opção de job years)
*Employee
(foi colocado o atributo jobYears - linha 36, colocado no construtor - linha 43 e como argumento, no equals - linha 59, foi feito o get - linha 100 e o set -linha 102, tambem foi acrescentado a public String toString() - linha 194)
*app.js
(acrescentado na linha 44 -<th>jobYears</th> e na linha 62 - <td>{this.props.employee.jobYears}</td>)

* hg status
* hg add
* hg commit - m "Resolved #3 Add new field jobYears"
* hg push https://1211749isepipppt@helixteamhub.cloud/crimson-shade-24/projects/devops-21-22-atb-1211749/repositories/mercurial/devops-21-22-atb-1211749-Mercurial

Para verificar se as alterações foram efetudas fiz o seguinte:
* cd CA1_Mercurial
* cd basic
* mvnw spring-boot:run (no terminal)
* http://localhost:8080/, surge com a atualização efetuada (imagem abaixo)
![img_15.png](img_15.png)

**5.** Efetuadas validações para cada um dos atributos que constituem os empregados (first name, last name, description, job years) considerando o cenário null, empty e valores negativos (jobyears)
![img_16.png](img_16.png)
![img_17.png](img_17.png)
![img_18.png](img_18.png)
![img_19.png](img_19.png)
![img_20.png](img_20.png)

* hg status
* hg add
* hg commit -m "Resolved #4,add validations to create Employee"
* hg push https://1211749isepipppt@helixteamhub.cloud/crimson-shade-24/projects/devops-21-22-atb-1211749/repositories/mercurial/devops-21-22-atb-1211749-Mercurial

**6.** Efetuados testes para cada um dos atributos que constituem os empregados (cenário "null", "empty" e <0)
![img_21.png](img_21.png)
![img_22.png](img_22.png)
![img_23.png](img_23.png)
![img_24.png](img_24.png)

* hg status
* hg add
* hg commit -m "Resolved #5, add tests"
* hg push https://1211749isepipppt@helixteamhub.cloud/crimson-shade-24/projects/devops-21-22-atb-1211749/repositories/mercurial/devops-21-22-atb-1211749-Mercurial

**7.** Criar o tag v1.2.0
* hg tag v1.2.0
* hg tags
* hg status
* hg add
* hg commit -m "Resolved #6, add tag v1.2.0"
* hg push https://1211749isepipppt@helixteamhub.cloud/crimson-shade-24/projects/devops-21-22-atb-1211749/repositories/mercurial/devops-21-22-atb-1211749-Mercurial

**8.** Termino da 1ªparte
* hg status
* hg add
* hg commit -m "Resolved #7, end part1"
* hg push https://1211749isepipppt@helixteamhub.cloud/crimson-shade-24/projects/devops-21-22-atb-1211749/repositories/mercurial/devops-21-22-atb-1211749-Mercurial

**9.** Criar o tag ca1-part1
* hg tag ca1-part1
* hg tags
* hg status
* hg add
* hg commit -m "Add tag ca1-part1"
* hg push https://1211749isepipppt@helixteamhub.cloud/crimson-shade-24/projects/devops-21-22-atb-1211749/repositories/mercurial/devops-21-22-atb-1211749-Mercurial

Nota: No repositório a vista não é tão intuitiva, por exemplo ao contrário do bitbucket que mostra de imediato toda a atividade ("commits") na pagina principal. No Helix team hub a página inicial apenas mostra o último commit, para termos, acesso a todos os commits já realizados temos de ir ao separador Activity ou podemos optar consultar os mesmos pelo terminal usando o comando hg log.


**10.** Foram criados diferentes issues para tratar da 2ª parte do exercício:
![img_33.png](img_33.png)

**11.** Criar o branch email
* hg branches (Head -> default)
* hg commit -m "create new branch email, Resolved #8"
* hg push https://1211749isepipppt@helixteamhub.cloud/crimson-shade-24/projects/devops-21-22-atb-1211749/repositories/mercurial/devops-21-22-atb-1211749-Mercurial --new-branch
* hg branches (email, default (inactive))

**12.** Criar novo campo email
* hg status
* hg add
* hg commit -m "create new support field email, Resolved #9"
* hg push https://1211749isepipppt@helixteamhub.cloud/crimson-shade-24/projects/devops-21-22-atb-1211749/repositories/mercurial/devops-21-22-atb-1211749-Mercurial --new-branch

**Nota:** no código foram feitas as mesmas alterções que foram referidas anteriormente no exercicio com git.

**13.** Criar validações e testes
![img_34.png](img_34.png)
![img_35.png](img_35.png)
* hg status
* hg add
* hg commit -m "Add validations and tests, Resolved #10 and #11"
* hg push https://1211749isepipppt@helixteamhub.cloud/crimson-shade-24/projects/devops-21-22-atb-1211749/repositories/mercurial/devops-21-22-atb-1211749-Mercurial --new-branch

Para verificar se as alterações foram efetudas fiz o seguinte:
* cd basic
* mvnw spring-boot:run (no terminal)
* http://localhost:8080/, surge com a atualização efetuada (imagem abaixo)
![img_27.png](img_27.png)

**14.** Merge com default
* hg update default
* hg branches (email, default(inactive))
* hg merge email
* hg commit -m "merge with default,Resolved #12"
* hg push https://1211749isepipppt@helixteamhub.cloud/crimson-shade-24/projects/devops-21-22-atb-1211749/repositories/mercurial/devops-21-22-atb-1211749-Mercurial --new-branch
* hg branches (default, email(inactive))

**15.** Criar tag v1.3.0
* hg tag v1.3.0
* hg tags (v1.3.0,ca1-part1,v1.2.0,v1.1.0)
* hg status
* hg add
* hg commit -m "Add new tag v1.3.0, Resolved #12"
* hg push https://1211749isepipppt@helixteamhub.cloud/crimson-shade-24/projects/devops-21-22-atb-1211749/repositories/mercurial/devops-21-22-atb-1211749-Mercurial --new-branch

**16.** Criar novo branch fix-invalid-email
* hg branch fix-invalid-email
* hg branches (default, email(inactive))
* hg commit -m "Create new branch fix-invalid-email, Resolved #13"
* hg push https://1211749isepipppt@helixteamhub.cloud/crimson-shade-24/projects/devops-21-22-atb-1211749/repositories/mercurial/devops-21-22-atb-1211749-Mercurial --new-branch
* hg branches (fix-invalid-email,default e email(inactive))

**17.** Adicionadas validações que contemplam o simbolo "@" que fazem parte do email mais testes
![img_37.png](img_37.png)
![img_36.png](img_36.png)

* hg status
* hg add
* hg commit -m "Add validations and tests including "@", Resolved #14"
* hg push https://1211749isepipppt@helixteamhub.cloud/crimson-shade-24/projects/devops-21-22-atb-1211749/repositories/mercurial/devops-21-22-atb-1211749-Mercurial --new-branch

**18.** Merge com default e criar tag v1.3.1
* hg update default
* hg branches (fix-invalid-email,default e email (invalid))
* hg merge fix-invalid-email
* hg commit -m "merge with default, Resolved #15"
* hg push https://1211749isepipppt@helixteamhub.cloud/crimson-shade-24/projects/devops-21-22-atb-1211749/repositories/mercurial/devops-21-22-atb-1211749-Mercurial --new-branch

**19.** Criar tag v1.3.1
* hg tag v1.3.1
* hg tags (v1.3.1,v1.3.0,ca1-part1,v1.2.0,v1.1.0)
* hg status
* hg add
* hg commit -m "Add tag v1.3.1, Resolved #15"
* hg push https://1211749isepipppt@helixteamhub.cloud/crimson-shade-24/projects/devops-21-22-atb-1211749/repositories/mercurial/devops-21-22-atb-1211749-Mercurial --new-branch

**20.** Termino da parte 2 e tag ca1

* hg tag ca1
* hg tags (ca1,v1.3.1,v1.3.0,ca1-part1,v1.2.0,v1.1.0)
* hg status
* hg add
* hg commit -m "End the assignment part2, add tag ca1, Resolved #16"
* hg push https://1211749isepipppt@helixteamhub.cloud/crimson-shade-24/projects/devops-21-22-atb-1211749/repositories/mercurial/devops-21-22-atb-1211749-Mercurial --new-branch

**Nota:** Os issues foram fechados manualmente ao contrário do que acontece no bitbucket que são fechados à medida que são referenciados nos commits.

#**Análise da versão alternativa ao GIT**

##**Mercurial**
**Características...**
* É um sistema de controlo de versão (VCS), usado por equipas de desenvolvimento de software para gerenciar e rastrear mudanças em projetos.
* Foi lançado em 2005, desde enão perdeu popularidade no entanto ainda é usado por algumas organizações de desenvolvimento como Facebook, Mozilla, W3 tem uma cota de 2% no mercado VCS.
* Os sistemas operacionais que ele suporta são Unix-like, Windows e macOS.
* Alto desempenho e escalabilidade.
* Recursos avançados de branching e merging.
* Desenvolvimento colaborativo totalmente distribuído.
* Descentralizado.
* Manipula arquivos binários e de texto sem formatação com eficiência.
* Possui uma interface Web integrada.

**Vantagens...**
* Rápido e poderoso.
* Fácil de aprender.
* Leve e portátil.
* Conceitualmente simples.

**Desvantagens...**
* Todos os complementos devem ser escritos em Python.
* Não são permitidos check-outs parciais.
* Bastante problemático quando usado com extensões adicionais.

**Outras curiosidades...**
* É um código aberto.
* Gratuito.


##**Git**
**Características...**
* Fornece um forte suporte para o desenvolvimento não linear.
* Modelo de repositório distribuído.
* Compatível com sistemas e protocolos existentes como HTTP, FTP, ssh.
* Capaz de lidar eficientemente com projetos de pequeno e grande porte.
* Autenticação criptográfica da história.
* Estratégias de fusão conectáveis.
* Design baseado em Toolkit.

**Vantagens...**
* Desempenho super rápido e eficiente.
* Multiplataforma.
* Mudanças de código podem ser facilmente e claramente monitoradas.
* Fácil de manter e robusto.
* Oferece um incrível utilitário de linha de comando conhecido como git bash.
Também oferece GIT GUI onde podemos muito rapidamente re-scan, mudança de estado, assinar, commit e empurrar o código rapidamente com apenas alguns cliques.

**Desvantagens...**
* Histórico maior e complexo torna-se difícil de compreender.
* Não suporta expansão de palavra-chave e preservação de timestamp.

**Outras curiosidades...**
* É um código aberto.
* Gratuito.


##**Diferença entre Git e Mercurial**

* O Mercurial é uma ferramenta preferida principalmente quando há a necessidade de uma equipa pequena para um projeto.
* O Git é uma ferramenta de versão usada para controlar e modificar as alterações do aplicativo e usada para manter o ciclo de desenvolvimento de software.
* No Mercurial, o utilizador não tem acesso para alterar o histórico de versões (mas tem permissão para alterar o último commit).
* No Git o utilizador pode aceder às configurações e alterar o histórico de versões conforme os requisitos. 
* No Mercurial não existe suporte para a staging area.
* O Git fornece a funcionalidade de staging area, á a área onde podem ser vistos os commits que estarão lá para o próximo commit.
* O Mercurial fornece suporte para branching, é complexa e não tão forte como a estrutura fornecida pelo Git. No Git o utilizador pode criar facilmente um branch para um único projeto.
* O Mercurial é uma ferramenta que pode ser usada com muito mais facilidade, ao contrário do Git que é uma ferramenta mais complexa.
* Mercurial pode ser mais seguro para utilizadores menos experientes, o Git oferece maneiras de aumentar a segurança.
* Há uma grande diferença entre git e mercurial, a forma como representam cada commit. O git representa commits instantâneos, enquanto mercurial os representa como diffs.

**Nota:** Ao longo da execução do trabalho foram feitas várias atualizações ao ficheiro Readme.md, utilizando os seguintes comandos:
* git status
* git add .
* git commit -m "Add changes in Readme File"
* git push origin master 



   